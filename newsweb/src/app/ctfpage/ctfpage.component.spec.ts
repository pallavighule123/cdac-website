import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CtfpageComponent } from './ctfpage.component';

describe('CtfpageComponent', () => {
  let component: CtfpageComponent;
  let fixture: ComponentFixture<CtfpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CtfpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CtfpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
