import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntiativeUpdatedComponent } from './intiative-updated.component';

describe('IntiativeUpdatedComponent', () => {
  let component: IntiativeUpdatedComponent;
  let fixture: ComponentFixture<IntiativeUpdatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntiativeUpdatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntiativeUpdatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


