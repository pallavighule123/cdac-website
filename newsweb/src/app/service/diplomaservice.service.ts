import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DiplomaserviceService {

  //url = 'https://192.168.190.180:23000/user'
  url = `https://patna.cdac.in/user`
  constructor(
    private router: Router,
    private httpClient: HttpClient
  ) { }

  login(username: string, password: string) {
    const body = {
      username: username,
      password: password
    }

    //console.log("inside ctf service")
    console.log(body)
    return this.httpClient.post(this.url + '/signin', body)
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (sessionStorage['token']) {
      // user is already logged in
      // launch the component
      return true
    }

    // force user to login
    this.router.navigate(['/diplomapage'])

    // user has not logged in yet
    // stop launching the component
    return false 
  }


  /*onSubmit(email: string, username: string){
    const body = {
      email: email,
      username: username,
      

    }
    
    console.log("inside diploma service on submit")
    return this.httpClient.post(this.url + '/forgot-password', body)
  }*/

  onSubmit(body){
    console.log("inside diploma service on submit payment details")
    return this.httpClient.post(this.url + '/payment_details', body)
  }



  getProfile(){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    
    console.log("inside get profile in angular service")
    console.log(httpOptions)


    return this.httpClient.get(this.url + '/profile', httpOptions)

  }


  loadUsersForAdmin(){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + '/profiles', httpOptions)

  }



  onSubmitFlag(body){
    console.log(body)
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })

     
    };
    
    return this.httpClient.post(this.url + '/flagstatus',body,httpOptions)
  }



  signUp(body){
    //console.log("inside diploma service")
    //console.log(body)
    return this.httpClient.post(this.url+`/dec/register`,body)
  }




}
