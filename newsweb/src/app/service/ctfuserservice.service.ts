import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class CtfuserserviceService  implements CanActivate {
  url = `https://patna.cdac.in/user`  
  constructor( private router: Router,
    private httpClient: HttpClient) { }



    login(email: string, password: string) {
      const body = {
        email: email,
        password: password
      }
  
      console.log("inside ctf service")
      console.log(body)
      return this.httpClient.post(this.url + '/signin', body)
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      /*if (sessionStorage['token']) {
        // user is already logged in
        // launch the component
        return true
      }*/

     if (localStorage['token']) {
        // user is already logged in
        // launch the component
        return true
      }
      


  
      // force user to login
      this.router.navigate(['/blackcat'])
  
      // user has not logged in yet
      // stop launching the component
      return false 
    }


    onSubmit(email: string, username: string){
      const body = {
        email: email,
        username: username,
        

      }
      
      console.log("inside ctf service on submit")
      return this.httpClient.post(this.url + '/forgot-password', body)
    }


    getProfile(){
      const httpOptions = {
        headers: new HttpHeaders({
          token: localStorage['token']
            //token: localStorage['token']
        })
      };
      
      console.log("inside get profile in angular service")
      console.log(httpOptions)


      return this.httpClient.get(this.url + '/profile', httpOptions)

    }


    loadUsersForAdmin(){
      const httpOptions = {
        headers: new HttpHeaders({
          token: sessionStorage['token']
        })
      };
    
      console.log("inside get profile in angular service")
      console.log(httpOptions)
      return this.httpClient.get(this.url + '/profiles',httpOptions)

    }



    onSubmitFlag(body){
      console.log(body)
      const httpOptions = {
        headers: new HttpHeaders({
          token: localStorage['token']
        })

       
      };
      
      return this.httpClient.post(this.url + '/flagstatus',body,httpOptions)
    }



    onStart(){
      console.log("inside on start")
      const httpOptions = {
        headers: new HttpHeaders({
          token: localStorage['token']
        })

       
      };
      return this.httpClient.get(this.url + '/start',httpOptions)
    }


    updateFlagStatus(body){
      console.log(body)
      const httpOptions = {
        headers: new HttpHeaders({
          token: localStorage['token']
        })

       
      };
      
      return this.httpClient.post(this.url + '/updateuserflagstatus',body,httpOptions)
    }


    findUpadatedPoints(){
      console.log("inside on findUpadatedPoints")
      const httpOptions = {
        headers: new HttpHeaders({
          token: localStorage['token']
        })

       
      };
      return this.httpClient.get(this.url + '/points',httpOptions)
    }



 
}
