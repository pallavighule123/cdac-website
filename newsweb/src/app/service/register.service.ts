import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  url = `https://patna.cdac.in`
  constructor(private httpClient:HttpClient,) { }

  register(body){
    console.log(body)
    return this.httpClient.post(this.url+`/register`,body)
  }
}
