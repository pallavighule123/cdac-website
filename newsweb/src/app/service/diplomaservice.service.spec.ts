import { TestBed } from '@angular/core/testing';

import { DiplomaserviceService } from './diplomaservice.service';

describe('DiplomaserviceService', () => {
  let service: DiplomaserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiplomaserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
