import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class NewsapiservicesService {

  url = `http://localhost:3000/user`

  constructor( private _http:HttpClient) { }

  //url
  //newsApiUrl="https://newsapi.org/v2/everything?qInTitle=AI&apiKey=56d3f59e15d54ba5ab247d6a36dfaf92"

  newsApiUrl="http://api.mediastack.com/v1/news?qInTitle=AI&countries=in&languages=en&keywords=AI&access_key=c418a865bb3138bb54bdc3efa08ee36a"
  
  //techNewsApiUrl="https://newsapi.org/v2/everything?qInTitle=AI&apiKey=56d3f59e15d54ba5ab247d6a36dfaf92"

  techNewsApiUrl="http://api.mediastack.com/v1/news?qInTitle=AI&countries=us&languages=en&keywords=tennis&access_key=c418a865bb3138bb54bdc3efa08ee36a"

  topHeading():Observable<any>
  {
    const httpOptions = {
      headers: new HttpHeaders({
        
      })

    }
    return this._http.get(this.newsApiUrl)
    //return this._http.get(this.url+`/news`)
  }


  techNews():Observable<any>
  {
    const httpOptions = {
      headers: new HttpHeaders({

      })

    }
    return this._http.get(this.techNewsApiUrl)
   //return this._http.get(this.url+`/news`)
  }



}
