import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SubscribeService {
  

  //url = `https://192.168.190.180:22000/user`

  url = `https://patna.cdac.in/user`
  
  constructor(
    private httpClient:HttpClient,
  ) { }

  signUp(body){
    //console.log("inside subscribe service")
    //console.log(body)
    return this.httpClient.post(this.url+`/register`,body)
  }
  register(body){
    console.log(body)
    return this.httpClient.post(this.url+`/register`,body)
  }
}

