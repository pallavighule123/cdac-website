import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BlockchainserviceService {

  url = `https://patna.cdac.in/user`  
  constructor(
    private httpClient:HttpClient
    ) { }

    signUp(body){
      //console.log("inside blockchain service")
      //console.log(body)
      return this.httpClient.post(this.url+`/block_register`,body)
    }

    submitDetails(body){
      
      return this.httpClient.post(this.url+`/transaction_details`,body)
    }
}
