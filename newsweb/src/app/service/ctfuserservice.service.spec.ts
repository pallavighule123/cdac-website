import { TestBed } from '@angular/core/testing';

import { CtfuserserviceService } from './ctfuserservice.service';

describe('CtfuserserviceService', () => {
  let service: CtfuserserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CtfuserserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
