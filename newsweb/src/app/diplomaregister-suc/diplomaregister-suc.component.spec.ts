import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiplomaregisterSucComponent } from './diplomaregister-suc.component';

describe('DiplomaregisterSucComponent', () => {
  let component: DiplomaregisterSucComponent;
  let fixture: ComponentFixture<DiplomaregisterSucComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiplomaregisterSucComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiplomaregisterSucComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
