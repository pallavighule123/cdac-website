import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiplomaregisterComponent } from './diplomaregister.component';

describe('DiplomaregisterComponent', () => {
  let component: DiplomaregisterComponent;
  let fixture: ComponentFixture<DiplomaregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiplomaregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiplomaregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
