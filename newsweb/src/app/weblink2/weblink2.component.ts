import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weblink2',
  templateUrl: './weblink2.component.html',
  styleUrls: ['./weblink2.component.css']
})
export class Weblink2Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  btnClick= function () {
    this.router.navigateByUrl('/registration');
};

}
