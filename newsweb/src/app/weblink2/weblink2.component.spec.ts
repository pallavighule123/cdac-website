import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Weblink2Component } from './weblink2.component';

describe('Weblink2Component', () => {
  let component: Weblink2Component;
  let fixture: ComponentFixture<Weblink2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Weblink2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Weblink2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
