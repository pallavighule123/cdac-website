import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CyberInitiativeComponent } from './cyber-initiative.component';

describe('CyberInitiativeComponent', () => {
  let component: CyberInitiativeComponent;
  let fixture: ComponentFixture<CyberInitiativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CyberInitiativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CyberInitiativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
