import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CtfregisterComponent } from './ctfregister.component';

describe('CtfregisterComponent', () => {
  let component: CtfregisterComponent;
  let fixture: ComponentFixture<CtfregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CtfregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CtfregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
