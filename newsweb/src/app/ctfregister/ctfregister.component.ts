import { Component, OnInit } from '@angular/core';
import { SubscribeService } from './../service/subscribe.service';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-ctfregister',
  templateUrl: './ctfregister.component.html',
  styleUrls: ['./ctfregister.component.css']
})
export class CtfregisterComponent implements OnInit {
  [x: string]: any;


  occupations: string[] = ["Training", "Testing", "Research", "Development"];
  selectedOccupation:string ="";


  sectors:string[] = ["AI For Health","AI For Cybersecurity","AI For AgriTech","AI For Mobility","AI For Open Source","Conversational AI","Data Analytics" ];
  selectedSector:string="";

  users:string[] = ["Student","System Admin","IT Professional","Cyber Security Enthusiast","Other"]
  selectedUser:string="";


  countryList: Array<any> = [
    { name: 'India(+91)', cities: ['--Select Your State--','Andaman and Nicobar Islands','Andhra Pradesh','Arunachal Pradesh','Assam','Bihar','Chandigarh','Chhattisgarh','Dadra and Nagar Haveli','Daman and Diu','Delhi','Goa','Gujarat','Haryana','Himachal Pradesh','Jammu and Kashmir','Jharkhand','Karnataka','Kerala','Lakshadweep','Madhya Pradesh','Maharashtra','Manipur','Meghalaya','Mizoram','Nagaland','Orissa','Pondicherry','Punjab','Rajasthan','Sikkim','Tamil Nadu','Tripura','Uttar Pradesh','Uttaranchal','West Bengal'] },
    { name: 'Afghanistan(+93)', cities: ['--Select Your State--','Badakhshan','Badghis','Baghlan','Balkh','Bamian','Farah','Faryab','Ghazni','Ghowr','Helmand','Herat','Jowzjan','Kabol','Kandahar','Kapisa','Konar','Kondoz','Laghman','Lowgar','Nangarhar','Nimruz','Oruzgan','Paktia','Paktika','Parvan','Samangan','Sar-e Pol','Takhar','Vardak','Zabol'] },
    { name: 'Albania(+355)', cities: ['--Select Your State--','Berat','Bulqize','Delvine','Devoll (Bilisht)','Diber (Peshkopi)','Durres','Elbasan','Fier','Gjirokaster','Gramsh','Has (Krume)','Kavaje','Kolonje (Erseke)','Korce','Kruje','Kucove','Kukes','Kurbin','Lezhe','Librazhd','Lushnje','Malesi e Madhe (Koplik)','Mallakaster (Ballsh)','Mat (Burrel)','Mirdite (Rreshen)','Peqin','Permet','Pogradec','Puke','Sarande','Shkoder','Skrapar (Corovode)','Tepelene','Tirane (Tirana)','Tirane (Tirana)','Tropoje (Bajram Curri)','Vlore'] },
    { name: 'Algeria(+213)', cities: ['--Select Your State--','Adrar','Ain Defla','Ain Temouchent','Alger','Annaba','Batna','Bechar','Bejaia','Biskra','Blida','Bordj Bou Arreridj','Bouira','Boumerdes','Chlef','Constantine','Djelfa','El Bayadh','El Oued','El Tarf','Ghardaia','Guelma','Illizi','Jijel','Khenchela','Laghouat','M "Sila','Mascara','Medea','Mila','Mostaganem','Naama','Oran','Ouargla','Oum el Bouaghi','Relizane','Saida','Setif','Sidi Bel Abbes','Skikda','Souk Ahras','Tamanghasset','Tebessa','Tiaret','Tindouf','Tipaza','Tissemsilt','Tizi Ouzou','Tlemcen'] },
    { name: 'American Samoa(+1-684)', cities: ['--Select Your State--','Eastern','Manu a','Rose Island','Swains Island','Western'] },
    { name: 'Angola(+244)', cities: ['--Select Your State--','Andorra la Vella','Bengo','Benguela','Bie','Cabinda','Canillo','Cuando Cubango','Cuanza Norte','Cuanza Sul','Cunene','Encamp','Escaldes-Engordany','Huambo','Huila','La Massana','Luanda','Lunda Norte','Lunda Sul','Malanje','Moxico','Namibe','Ordino','Sant Julia de Loria','Uige','Zaire'] },
    { name: 'Anguilla(+1-264)', cities: ['--Select Your State--','Anguilla'] },
    { name: 'Antartica(+672)', cities: ['--Select Your State--','Antartica'] },
    { name: 'Antigua and Barbuda(+1-268)', cities: ['--Select Your State--','Barbuda','Redonda','Saint George','Saint John','Saint Mary','Saint Paul','Saint Peter','Saint Philip'] },
    { name: 'Argentina(+54)', cities: ['--Select Your State--','Antartica e Islas del Atlantico Sur','Buenos Aires','Buenos Aires Capital Federal','Catamarca','Chaco','Chubut','Cordoba','Corrientes','Entre Rios','Formosa','Jujuy','La Pampa','La Rioja','Mendoza','Misiones','Neuquen','Rio Negro','Salta','San Juan','San Luis','Santa Cruz','Santa Fe','Santiago del Estero','Tierra del Fuego','Tucuman'] },
    { name: 'Armenia(+374)', cities: ['--Select Your State--','Aragatsotn','Ararat','Armavir','Geghark "unik"','Kotayk','Lorri','Shirak','Syunik','Tavush','Vayots "Dzor"','Yerevan'] },
    { name: 'Aruba(+297)', cities: ['--Select Your State--','Aruba'] },
    { name: 'Ashmore and Cartier Island(+93)', cities: ['--Select Your State--','Ashmore and Cartier Island'] },
    { name: 'Australia(+61)', cities: ['--Select Your State--','Australian Capital Territory','New South Wales','Northern Territory','Queensland','South Australia','Tasmania','Victoria','Western Australia'] },
    { name: 'Austria(+43)', cities: ['--Select Your State--','Burgenland','Kaernten','Niederoesterreich','Oberoesterreich','Salzburg','Steiermark','Tirol','Vorarlberg','Wien'] },
    { name: 'Azerbaijan(+994)', cities: ['--Select Your State--','Abseron Rayonu','Agcabadi Rayonu','Agdam Rayonu','Agdas Rayonu','Agstafa Rayonu','Agsu Rayonu','Ali Bayramli Sahari','Astara Rayonu','Baki Sahari','Balakan Rayonu','Barda Rayonu','Beylaqan Rayonu','Bilasuvar Rayonu','Cabrayil Rayonu','Calilabad Rayonu','Daskasan Rayonu','Davaci Rayonu','Fuzuli Rayonu','Gadabay Rayonu','Ganca Sahari','Goranboy Rayonu','Goycay Rayonu','Haciqabul Rayonu','Imisli Rayonu','Ismayilli Rayonu','Kalbacar Rayonu','Kurdamir Rayonu','Lacin Rayonu','Lankaran Rayonu','Lankaran Sahari','Lerik Rayonu','Masalli Rayonu','Mingacevir Sahari','Naftalan Sahari','Naxcivan Muxtar Respublikasi','Neftcala Rayonu','Oguz Rayonu','Qabala Rayonu','Qax Rayonu','Qazax Rayonu','Qobustan Rayonu','Quba Rayonu','Qubadli Rayonu','Qusar Rayonu','Saatli Rayonu','Sabirabad Rayonu','Saki Rayonu','Saki Sahari','Salyan Rayonu','Samaxi Rayonu','Samkir Rayonu','Samux Rayonu','Siyazan Rayonu','Sumqayit Sahari','Susa Rayonu','Susa Sahari','Tartar Rayonu','Tovuz Rayonu','Ucar Rayonu','Xacmaz Rayonu','Xankandi Sahari','Xanlar Rayonu','Xizi Rayonu','Xocali Rayonu','Xocavand Rayonu','Yardimli Rayonu','Yevlax Rayonu','Yevlax Sahari','Zangilan Rayonu','Zaqatala Rayonu','Zardab Rayonu'] },
    { name: 'Bahamas(+1-242)', cities: ['--Select Your State--','Acklins and Crooked Islands','Bimini','Cat Island','Exuma','Freeport','Fresh Creek','Governor"s Harbour','Green Turtle Cay','Harbour Island','High Rock','Inagua','Kemps Bay','Long Island','Marsh Harbour','Mayaguana','New Providence','Nicholls Town and Berry Islands','Ragged Island','Rock Sound','San Salvador and Rum Cay','Sandy Point'] },
    { name: 'Bahrain(+973)', cities: ['--Select Your State--','Al Hadd','Al Manamah','Al Mintaqah al Gharbiyah','Al Mintaqah al Wusta','Al Mintaqah ash Shamaliyah','Al Muharraq','Ar Rifa" wa al Mintaqah al Janubiyah','Jidd Hafs','Juzur Hawar','Madinat "Isa','Madinat Hamad','Sitrah'] },
    { name: 'Bangladesh(+880)', cities: ['--Select Your State--','Barguna','Barisal','Bhola','Jhalokati','Patuakhali','Pirojpur','Bandarban','Brahmanbaria','Chandpur','Chittagong','Comilla','Cox "s Bazar','Feni','Khagrachari','Lakshmipur','Noakhali','Rangamati','Dhaka','Faridpur','Gazipur','Gopalganj','Jamalpur','Kishoreganj','Madaripur','Manikganj','Munshiganj','Mymensingh','Narayanganj','Narsingdi','Netrokona','Rajbari','Shariatpur','Sherpur','Tangail','Bagerhat','Chuadanga','Jessore','Jhenaidah','Khulna','Kushtia','Magura','Meherpur','Narail','Satkhira','Bogra','Dinajpur','Gaibandha','Jaipurhat','Kurigram','Lalmonirhat','Naogaon','Natore','Nawabganj','Nilphamari','Pabna','Panchagarh','Rajshahi','Rangpur','Sirajganj','Thakurgaon','Habiganj','Maulvi bazar','Sunamganj','Sylhet'] },
    { name: 'Barbados', cities: ['--Select Your State--','Bridgetown','Christ Church','Saint Andrew','Saint George','Saint James','Saint John','Saint Joseph','Saint Lucy','Saint Michael','Saint Peter','Saint Philip','Saint Thomas'] },
    { name: 'Belarus', cities: ['--Select Your State--','Brestskaya (Brest)','Homyel"skaya (Homyel)','Horad Minsk','Hrodzyenskaya (Hrodna)','Mahilyowskaya (Mahilyow)','Minskaya','Vitsyebskaya (Vitsyebsk)'] },
    { name: 'Belgium', cities: ['--Select Your State--','Antwerpen','Brabant Wallon','Brussels Capitol Region','Hainaut','Liege','Limburg','Luxembourg','Namur','Oost-Vlaanderen','Vlaams Brabant','West-Vlaanderen'] },
    { name: 'Belize', cities: ['--Select Your State--','Belize','Cayo','Corozal','Orange Walk','Stann Creek','Toledo'] },
    { name: 'Benin', cities: ['--Select Your State--','Alibori','Atakora','Atlantique','Borgou','Collines','Couffo','Donga','Littoral','Mono','Oueme','Plateau','Zou'] },
    { name: 'Bermuda', cities: ['--Select Your State--','Devonshire','Hamilton','Hamilton','Paget','Pembroke','Saint George','Saint Georges','Sandys','Smiths','Southampton','Warwick'] },
    { name: 'Bhutan', cities: ['--Select Your State--','Bumthang','Chhukha','Chirang','Daga','Geylegphug','Ha','Lhuntshi','Mongar','Paro','Pemagatsel','Punakha','Samchi','Samdrup Jongkhar','Shemgang','Tashigang','Thimphu','Tongsa','Wangdi Phodrang'] },
    { name: 'Bolivia', cities: ['--Select Your State--','Beni','Chuquisaca','Cochabamba','La Paz','Oruro','Pando','Potosi','Santa Cruz','Tarija'] },
    { name: 'Bosnia and Herzegovina', cities: ['--Select Your State--','Federation of Bosnia and Herzegovina','Republika Srpska'] },
    { name: 'Botswana', cities: ['--Select Your State--','Central','Chobe','Francistown','Gaborone','Ghanzi','Kgalagadi','Kgatleng','Kweneng','Lobatse','Ngamiland','North-East','Selebi-Pikwe','South-East','Southern'] },
    { name: 'Brazil', cities: ['--Select Your State--','Acre','Alagoas','Amapa','Amazonas','Bahia','Ceara','Distrito Federal','Espirito Santo','Goias','Maranhao','Mato Grosso','Mato Grosso do Sul','Minas Gerais','Para','Paraiba','Parana','Pernambuco','Piaui','Rio de Janeiro','Rio Grande do Norte','Rio Grande do Sul','Rondonia','Roraima','Santa Catarina','Sao Paulo','Sergipe','Tocantins'] },
    { name: 'British Virgin Islands', cities: ['--Select Your State--','Anegada','Jost Van Dyke','Tortola','Virgin Gorda'] },
    { name: 'Brunei', cities: ['--Select Your State--','Belait','Brunei and Muara','Temburong','Tutong'] },
    { name: 'Bulgaria', cities: ['--Select Your State--','Blagoevgrad','Burgas','Dobrich','Gabrovo','Khaskovo','Kurdzhali','Kyustendil','Lovech','Montana','Pazardzhik','Pernik','Pleven','Plovdiv','Razgrad','Ruse','Shumen','Silistra','Sliven','Smolyan','Sofiya','Sofiya-Grad','Stara Zagora','Turgovishte','Varna','Veliko Turnovo','Vidin','Vratsa','Yambol'] },
    { name: 'Burkina Faso', cities: ['--Select Your State--','Bale','Bam','Banwa','Bazega','Bougouriba','Boulgou','Boulkiemde','Comoe','Ganzourgou','Gnagna','Gourma','Houet','Ioba','Kadiogo','Kenedougou','Komandjari','Kompienga','Kossi','Koupelogo','Kouritenga','Kourweogo','Leraba','Loroum','Mouhoun','Nahouri','Namentenga','Naumbiel','Nayala','Oubritenga','Oudalan','Passore','Poni','Samentenga','Sanguie','Seno','Sissili','Soum','Sourou','Tapoa','Tuy','Yagha','Yatenga','Ziro','Zondomo','Zoundweogo'] },
    { name: 'Burma', cities: ['--Select Your State--','Ayeyarwady','Bago','Chin State','Kachin State','Kayah State','Kayin State','Magway','Mandalay','Mon State','Rakhine State','Sagaing','Shan State','Tanintharyi','Yangon'] },
    { name: 'Burundi', cities: ['--Select Your State--','Bubanza','Bujumbura','Bururi','Cankuzo','Cibitoke','Gitega','Karuzi','Kayanza','Kirundo','Makamba','Muramvya','Muyinga','Mwaro','Ngozi','Rutana','Ruyigi'] },
    { name: 'Cambodia', cities: ['--Select Your State--','Banteay Mean Cheay','Batdambang','Kampong Cham','Kampong Chhnang','Kampong Spoe','Kampong Thum','Kampot','Kandal','Kaoh Kong','Keb','Kracheh','Mondol Kiri','Otdar Mean Cheay','Pailin','Phnum Penh','Pouthisat','Preah Seihanu (Sihanoukville)','Preah Vihear','Prey Veng','Rotanah Kiri','Siem Reab','Stoeng Treng','Svay Rieng','Takev'] },
    { name: 'Cameroon', cities: ['--Select Your State--','Adamaoua','Centre','Est','Extreme-Nord','Littoral','Nord','Nord-Ouest','Ouest','Sud','Sud-Ouest'] },
    { name: 'Canada', cities: ['--Select Your State--','Alberta','British Columbia','Manitoba','New Brunswick','Newfoundland','Northwest Territories','Nova Scotia','Nunavut','Ontario','Prince Edward Island','Quebec','Saskatchewan','Yukon Territory'] },
    { name: 'Cape Verde', cities: ['--Select Your State--','Boa Vista','Brava','Maio','Mosteiros','Paul','Porto Novo','Praia','Ribeira Grande','Sal','Santa Catarina','Santa Cruz','Sao Domingos','Sao Filipe','Sao Nicolau','Sao Vicente','Tarrafal'] },
    { name: 'Cayman Islands', cities: ['--Select Your State--','Creek','Eastern','Midland','South Town','Spot Bay','Stake Bay','West End','Western'] },
    { name: 'Central African Republic', cities: ['--Select Your State--','Bamingui-Bangoran','Bangui','Basse-Kotto','Gribingui','Haut-Mbomou','Haute-Kotto','Haute-Sangha','Kemo-Gribingui','Lobaye','Mbomou','Nana-Mambere','Ombella-Mpoko','Ouaka','Ouham','Ouham-Pende','Sangha','Vakaga'] },
    { name: 'Chad', cities: ['--Select Your State--','Batha','Biltine','Borkou-Ennedi-Tibesti','Chari-Baguirmi','Guera','Kanem','Lac','Logone Occidental','Logone Oriental','Mayo-Kebbi','Moyen-Chari','Ouaddai','Salamat','Tandjile'] },
    { name: 'Chile', cities: ['--Select Your State--','Aisen del General Carlos Ibanez del Campo','Antofagasta','Araucania','Atacama','Bio-Bio','Coquimbo','Libertador General Bernardo O"Higgins','Los Lagos','Magallanes y de la Antartica Chilena','Maule','Region Metropolitana (Santiago)','Tarapaca','Valparaiso'] },
    { name: 'China', cities: ['--Select Your State--','Anhui','Beijing','Chongqing','Fujian','Gansu','Guangdong','Guangxi','Guizhou','Hainan','Hebei','Heilongjiang','Henan','Hubei','Hunan','Jiangsu','Jiangxi','Jilin','Liaoning','Nei Mongol','Ningxia','Qinghai','Shaanxi','Shandong','Shanghai','Shanxi','Sichuan','Tianjin','Xinjiang','Xizang (Tibet)','Yunnan','Zhejiang'] },
    { name: 'Christmas Island', cities: ['--Select Your State--','Christmas Island'] },
    { name: 'Clipperton Island', cities: ['--Select Your State--','Clipperton Island'] },
    { name: 'Cocos (Keeling) Islands', cities: ['--Select Your State--','Direction Island','Home Island','Horsburgh Island','North Keeling Island','South Island','West Island'] },
    { name: 'Colombia', cities: ['--Select Your State--','Amazonas','Antioquia','Arauca','Atlantico','Bolivar','Boyaca','Caldas','Caqueta','Casanare','Cauca','Cesar','Choco','Cordoba','Cundinamarca','Distrito Capital de Santa Fe de Bogota','Guainia','Guaviare','Huila','La Guajira','Magdalena','Meta','Narino','Norte de Santander','Putumayo','Quindio','Risaralda','San Andres y Providencia','Santander','Sucre','Tolima','Valle del Cauca','Vaupes','Vichada'] },
    { name: 'Comoros', cities: ['--Select Your State--','Anjouan (Nzwani)','Domoni','Fomboni','Grande Comore (Njazidja)','Moheli (Mwali)','Moroni','Moutsamoudou'] },
    { name: 'Congo,Democratic Republic of the', cities: ['--Select Your State--','Bandundu','Bas-Congo','Equateur','Kasai-Occidental','Kasai-Oriental','Katanga','Kinshasa','Maniema','Nord-Kivu','Orientale','Sud-Kivu'] },
    { name: 'Congo, Republic of the', cities: ['--Select Your State--','Bouenza','Brazzaville','Cuvette','Kouilou','Lekoumou','Likouala','Niari','Plateaux','Pool','Sangha'] },
    { name: 'Cook Islands', cities: ['--Select Your State--','Aitutaki','Atiu','Avarua','Mangaia','Manihiki','Manuae','Mauke','Mitiaro','Nassau Island','Palmerston','Penrhyn','Pukapuka','Rakahanga','Rarotonga','Suwarrow','Takutea'] },
    { name: 'Costa Rica', cities: ['--Select Your State--','Alajuela','Cartago','Guanacaste','Heredia','Limon','Puntarenas','San Jose'] },
    { name: 'Cote d"Ivoire', cities: ['--Select Your State--','Abengourou','Abidjan','Aboisso','Adiake','Adzope','Agboville','Agnibilekrou','Ale"pe"','Bangolo','Beoumi','Biankouma','Bocanda','Bondoukou','Bongouanou','Bouafle','Bouake','Bouna','Boundiali','Dabakala','Dabon','Daloa','Danane','Daoukro','Dimbokro','Divo','Duekoue','Ferkessedougou','Gagnoa','Grand Bassam','Grand-Lahou','Guiglo','Issia','Jacqueville','Katiola','Korhogo','Lakota','Man','Mankono','Mbahiakro','Odienne','Oume','Sakassou','San-Pedro','Sassandra','Seguela','Sinfra','Soubre','Tabou','Tanda','Tiassale','Tiebissou','Tingrela','Touba','Toulepleu','Toumodi','Vavoua','Yamoussoukro','Zuenoula'] },
    { name: 'Croatia', cities: ['--Select Your State--','Bjelovarsko-Bilogorska Zupanija','Brodsko-Posavska Zupanija','Dubrovacko-Neretvanska Zupanija','Istarska Zupanija','Karlovacka Zupanija','Koprivnicko-Krizevacka Zupanija','Krapinsko-Zagorska Zupanija','Licko-Senjska Zupanija','Medimurska Zupanija','Osjecko-Baranjska Zupanija','Pozesko-Slavonska Zupanija','Primorsko-Goranska Zupanija','Sibensko-Kninska Zupanija','Sisacko-Moslavacka Zupanija','Splitsko-Dalmatinska Zupanija','Varazdinska Zupanija','Viroviticko-Podravska Zupanija','Vukovarsko-Srijemska Zupanija','Zadarska Zupanija','Zagreb','Zagrebacka Zupanija'] },
    { name: 'Cuba', cities: ['--Select Your State--','Camaguey','Ciego de Avila','Cienfuegos','Ciudad de La Habana','Granma','Guantanamo','Holguin','Isla de la Juventud','La Habana','Las Tunas','Matanzas','Pinar del Rio','Sancti Spiritus','Santiago de Cuba','Villa Clara'] },
    { name: 'Cyprus', cities: ['--Select Your State--','Famagusta','Kyrenia','Larnaca','Limassol','Nicosia','Paphos'] },
    { name: 'Czeck Republic', cities: ['--Select Your State--','Brnensky','Budejovicky','Jihlavsky','Karlovarsky','Kralovehradecky','Liberecky','Olomoucky','Ostravsky','Pardubicky','Plzensky','Praha','Stredocesky','Ustecky','Zlinsky'] },
    { name: 'Denmark', cities: ['--Select Your State--','Arhus','Bornholm','Fredericksberg','Frederiksborg','Fyn','Kobenhavn','Kobenhavns','Nordjylland','Ribe','Ringkobing','Roskilde','Sonderjylland','Storstrom','Vejle','Vestsjalland','Viborg'] },
    { name: 'Djibouti', cities: ['--Select Your State--','Ali Sabih','Dikhil','Djibouti','Obock','Tadjoura'] },
    { name: 'Dominica', cities: ['--Select Your State--','Saint Andrew','Saint David','Saint George','Saint John','Saint Joseph','Saint Luke','Saint Mark','Saint Patrick','Saint Paul','Saint Peter'] },
    { name: 'Dominican Republic', cities: ['--Select Your State--','Azua','Baoruco','Barahona','Dajabon','Distrito Nacional','Duarte','El Seibo','Elias Pina','Espaillat','Hato Mayor','Independencia','La Altagracia','La Romana','La Vega','Maria Trinidad Sanchez','Monsenor Nouel','Monte Cristi','Monte Plata','Pedernales','Peravia','Puerto Plata','Salcedo','Samana','San Cristobal','San Juan','San Pedro de Macoris','Sanchez Ramirez','Santiago','Santiago Rodriguez','Valverde'] },
    { name: 'Ecuador', cities: ['--Select Your State--','Azuay','Bolivar','Canar','Carchi','Chimborazo','Cotopaxi','El Oro','Esmeraldas','Galapagos','Guayas','Imbabura','Loja','Los Rios','Manabi','Morona-Santiago','Napo','Orellana','Pastaza','Pichincha','Sucumbios','Tungurahua','Zamora-Chinchipe'] },
    { name: 'Egypt', cities: ['--Select Your State--','Ad Daqahliyah','Al Bahr al Ahmar','Al Buhayrah','Al Fayyum','Al Gharbiyah','Al Iskandariyah','Al Isma"iliyah','Al Jizah','Al Minufiyah','Al Minya','Al Qahirah','Al Qalyubiyah','Al Wadi al Jadid','As Suways','Ash Sharqiyah','Aswan','Asyut','Bani Suwayf','Bur Sa"id','Dumyat','Janub Sina','Kafr ash Shaykh','Matruh','Qina','Shamal Sina','Suhaj'] },
    { name: 'El Salvador', cities: ['--Select Your State--','Ahuachapan','Cabanas','Chalatenango','Cuscatlan','La Libertad','La Paz','La Union','Morazan','San Miguel','San Salvador','San Vicente','Santa Ana','Sonsonate','Usulutan'] },
    { name: 'Equatorial Guinea', cities: ['--Select Your State--','Annobon','Bioko Norte','Bioko Sur','Centro Sur','Kie-Ntem','Litoral','Wele-Nzas'] },
    { name: 'Eritrea', cities: ['--Select Your State--','Akale Guzay','Barka','Denkel','Hamasen','Sahil','Semhar','Senhit','Seraye'] },
    { name: 'Estonia', cities: ['--Select Your State--','Harjumaa (Tallinn)','Hiiumaa (Kardla)','Ida-Virumaa (Johvi)','Jarvamaa (Paide)','Jogevamaa (Jogeva)','Laane-Virumaa (Rakvere)','Laanemaa (Haapsalu)','Parnumaa (Parnu)','Polvamaa (Polva)','Raplamaa (Rapla)','Saaremaa (Kuessaare)','Tartumaa (Tartu)','Valgamaa (Valga)','Viljandimaa (Viljandi)','Vorumaa (Voru)'] },
    { name: 'Ethiopia', cities: ['--Select Your State--','Adis Abeba (Addis Ababa)','Afar','Amara','Dire Dawa','Gambela Hizboch','Hareri Hizb','Oromiya','Sumale','Tigray','YeDebub Biheroch Bihereseboch na Hizboch'] },
    { name: '"Europa Island', cities: ['--Select Your State--','Europa Island'] },
    { name: 'Falkland Islands (Islas Malvinas)', cities: ['--Select Your State--','Falkland Islands (Islas Malvinas)'] },
    { name: 'Faroe Islands', cities: ['--Select Your State--','Bordoy','Eysturoy','Mykines','Sandoy','Skuvoy','Streymoy','Suduroy','Tvoroyri','Vagar'] },
    { name: 'Fiji', cities: ['--Select Your State--','Central','Eastern','Northern','Rotuma','Western'] },
    { name: 'Finland', cities: ['--Select Your State--','Aland','Etela-Suomen Laani','Ita-Suomen Laani','Lansi-Suomen Laani','Lappi','Oulun Laani'] },
    { name: 'France', cities: ['--Select Your State--','Alsace','Aquitaine','Auvergne','Basse-Normandie','Bourgogne','Bretagne','Centre','Champagne-Ardenne','Corse','Franche-Comte','Haute-Normandie','Ile-de-France','Languedoc-Roussillon','Limousin','Lorraine','Midi-Pyrenees','Nord-Pas-de-Calais','Pays de la Loire','Picardie','Poitou-Charentes','Provence-Alpes-Cote d"Azur','Rhone-Alpes'] },
    { name: 'French Guiana', cities: ['--Select Your State--','French Guiana'] },
    { name: 'French Polynesia",', cities: ['--Select Your State--','Archipel des Marquises','Archipel des Tuamotu','Archipel des Tubuai','Iles du Vent','Iles Sous-le-Vent'] },
    { name: 'French Southern and Antarctic Lands', cities: ['--Select Your State--','Adelie Land','Ile Crozet','Iles Kerguelen','Iles Saint-Paul et Amsterdam'] },
    { name: 'Gabon', cities: ['--Select Your State--','Estuaire','Haut-Ogooue','Moyen-Ogooue','Ngounie','Nyanga','Ogooue-Ivindo','Ogooue-Lolo','Ogooue-Maritime','Woleu-Ntem'] },
    { name: 'Gambia, The', cities: ['--Select Your State--','Banjul','Central River','Lower River','North Bank','Upper River','Western'] },
    { name: 'Gaza Strip', cities: ['--Select Your State--','Gaza Strip'] },
    { name: 'Georgia', cities: ['--Select Your State--','Abashis','Abkhazia or Ap"khazet"is Avtonomiuri Respublika (Sokhumi)','Adigenis','Ajaria or Acharis Avtonomiuri Respublika (Bat"umi)','Akhalgoris','Akhalk"alak"is','Akhalts"ikhis','Akhmetis','Ambrolauris','Aspindzis','Baghdat"is','Bolnisis','Borjomis','Ch"khorotsqus','Ch"okhatauris','Chiat"ura','Dedop"listsqaros','Dmanisis','Dushet"is','Gardabanis','Gori','Goris','Gurjaanis','Javis','K"arelis','K"ut"aisi','Kaspis','Kharagaulis','Khashuris','Khobis','Khonis','Lagodekhis','Lanch"khut"is','Lentekhis','Marneulis','Martvilis','Mestiis','Mts"khet"is','Ninotsmindis','Onis','Ozurget"is','P"ot"i','Qazbegis','Qvarlis','Rust"avi','Sach"kheris','Sagarejos','Samtrediis','Senakis','Sighnaghis','T"bilisi','T"elavis','T"erjolis','T"et"ritsqaros','T"ianet"is','Tqibuli','Ts"ageris','Tsalenjikhis','Tsalkis','Tsqaltubo','Vanis','Zestap"onis','Zugdidi','Zugdidis'] },
    { name: 'Germany', cities: ['--Select Your State--','Baden-Wuerttemberg','Bayern','Berlin','Brandenburg','Bremen','Hamburg','Hessen','Mecklenburg-Vorpommern','Niedersachsen','Nordrhein-Westfalen','Rheinland-Pfalz','Saarland','Sachsen','Sachsen-Anhalt','Schleswig-Holstein','Thueringen'] },
    { name: 'Ghana', cities: ['--Select Your State--','Ashanti','Brong-Ahafo','Central','Eastern','Greater Accra','Northern','Upper East','Upper West','Volta','Western'] },
    { name: 'Gibraltar', cities: ['--Select Your State--','Gibraltar'] },
    { name: 'Glorioso Islands', cities: ['--Select Your State--','Ile du Lys','Ile Glorieuse'] },
    { name: 'Greece', cities: ['--Select Your State--','Aitolia kai Akarnania','Akhaia','Argolis','Arkadhia','Arta','Attiki','Ayion Oros (Mt. Athos)','Dhodhekanisos','Drama','Evritania','Evros','Evvoia','Florina','Fokis','Fthiotis','Grevena','Ilia','Imathia','Ioannina','Irakleion','Kardhitsa','Kastoria','Kavala','Kefallinia','Kerkyra','Khalkidhiki','Khania','Khios','Kikladhes','Kilkis','Korinthia','Kozani','Lakonia','Larisa','Lasithi','Lesvos','Levkas','Magnisia','Messinia','Pella','Pieria','Preveza','Rethimni','Rodhopi','Samos','Serrai','Thesprotia','Thessaloniki','Trikala','Voiotia','Xanthi','Zakinthos'] },
    { name: 'Greenland', cities: ['--Select Your State--','Avannaa (Nordgronland)','Kitaa (Vestgronland)','Tunu (Ostgronland)'] },
    { name: 'Grenada', cities: ['--Select Your State--','Carriacou and Petit Martinique','Saint Andrew','Saint David','Saint George','Saint John','Saint Mark','Saint Patrick'] },
    { name: 'Guadeloupe', cities: ['--Select Your State--','Basse-Terre','Grande-Terre','Iles de la Petite Terre','Iles des Saintes','Marie-Galante'] },
    { name: 'Guam', cities: ['--Select Your State--','Guam'] },
    { name: 'Guatemala', cities: ['--Select Your State--','Alta Verapaz','Baja Verapaz','Chimaltenango','Chiquimula','El Progreso','Escuintla','Guatemala','Huehuetenango','Izabal','Jalapa','Jutiapa','Peten','Quetzaltenango','Quiche','Retalhuleu','Sacatepequez','San Marcos','Santa Rosa','Solola','Suchitepequez','Totonicapan','Zacapa'] },
    { name: 'Guernsey', cities: ['--Select Your State--','Castel','Forest','St. Andrew','St. Martin','St. Peter Port','St. Pierre du Bois','St. Sampson','St. Saviour','Torteval','Vale'] },
    { name: 'Guinea', cities: ['--Select Your State--','Beyla','Boffa','Boke','Conakry','Coyah','Dabola','Dalaba','Dinguiraye','Dubreka','Faranah','Forecariah','Fria','Gaoual','Gueckedou','Kankan','Kerouane','Kindia','Kissidougou','Koubia','Koundara','Kouroussa','Labe','Lelouma','Lola','Macenta','Mali','Mamou','Mandiana','Nzerekore','Pita','Siguiri','Telimele','Tougue','Yomou'] },
    { name: 'Guinea-Bissau', cities: ['--Select Your State--','Bafata','Biombo','Bissau','Bolama-Bijagos','Cacheu','Gabu','Oio','Quinara','Tombali'] },
    { name: 'Guyana', cities: ['--Select Your State--','Barima-Waini','Cuyuni-Mazaruni','Demerara-Mahaica','East Berbice-Corentyne','Essequibo Islands-West Demerara','Mahaica-Berbice','Pomeroon-Supenaam','Potaro-Siparuni','Upper Demerara-Berbice','Upper Takutu-Upper Essequibo'] },
    { name: 'Haiti', cities: ['--Select Your State--','Artibonite','Centre','Grand"Anse','Nord','Nord-Est','Nord-Ouest','Ouest','Sud','Sud-Est'] },
    { name: 'Heard Island and McDonald Islands', cities: ['--Select Your State--','Heard Island and McDonald Islands'] },
    { name: 'Holy See (Vatican City)', cities: ['--Select Your State--','Holy See (Vatican City)'] },
    { name: 'Honduras', cities: ['--Select Your State--','Atlantida','Choluteca','Colon','Comayagua','Copan','Cortes','El Paraiso','Francisco Morazan','Gracias a Dios','Intibuca','Islas de la Bahia','La Paz','Lempira','Ocotepeque','Olancho','Santa Barbara','Valle','Yoro'] },
    { name: 'Hong Kong",', cities: ['--Select Your State--','Hong Kong'] },
    { name: 'Howland Island', cities: ['--Select Your State--','Howland Island'] },
    { name: 'Hungary', cities: ['--Select Your State--','Bacs-Kiskun','Baranya','Bekes','Bekescsaba','Borsod-Abauj-Zemplen','Budapest','Csongrad','Debrecen','Dunaujvaros','Eger','Fejer','Gyor','Gyor-Moson-Sopron','Hajdu-Bihar','Heves','Hodmezovasarhely','Jasz-Nagykun-Szolnok','Kaposvar','Kecskemet','Komarom-Esztergom','Miskolc','Nagykanizsa','Nograd','Nyiregyhaza','Pecs','Pest','Somogy','Sopron','Szabolcs-Szatmar-Bereg','Szeged','Szekesfehervar','Szolnok','Szombathely','Tatabanya','Tolna','Vas','Veszprem','Veszprem','Zala','Zalaegerszeg'] },
    { name: 'Iceland', cities: ['--Select Your State--','Akranes','Akureyri','Arnessysla','Austur-Bardhastrandarsysla','Austur-Hunavatnssysla','Austur-Skaftafellssysla','Borgarfjardharsysla','Dalasysla','Eyjafjardharsysla','Gullbringusysla','Hafnarfjordhur','Husavik','Isafjordhur','Keflavik','Kjosarsysla','Kopavogur','Myrasysla','Neskaupstadhur','Nordhur-Isafjardharsysla','Nordhur-Mulasys-la','Nordhur-Thingeyjarsysla','Olafsfjordhur','Rangarvallasysla','Reykjavik','Saudharkrokur','Seydhisfjordhur','Siglufjordhur','Skagafjardharsysla','Snaefellsnes-og Hnappadalssysla','Strandasysla','Sudhur-Mulasysla','Sudhur-Thingeyjarsysla','Vesttmannaeyjar','Vestur-Bardhastrandarsysla','Vestur-Hunavatnssysla','Vestur-Isafjardharsysla','Vestur-Skaftafellssysla'] },
    
    { name: 'Indonesia', cities: ['--Select Your State--','Aceh','Bali','Banten','Bengkulu','East Timor','Gorontalo','Irian Jaya','Jakarta Raya','Jambi','Jawa Barat','Jawa Tengah','Jawa Timur','Kalimantan Barat','Kalimantan Selatan','Kalimantan Tengah','Kalimantan Timur','Kepulauan Bangka Belitung','Lampung','Maluku','Maluku Utara','Nusa Tenggara Barat','Nusa Tenggara Timur','Riau','Sulawesi Selatan','Sulawesi Tengah','Sulawesi Tenggara','Sulawesi Utara','Sumatera Barat','Sumatera Selatan','Sumatera Utara','Yogyakarta'] },
    { name: 'Iran', cities: ['--Select Your State--','Ardabil','Azarbayjan-e Gharbi','Azarbayjan-e Sharqi','Bushehr','Chahar Mahall va Bakhtiari','Esfahan','Fars','Gilan','Golestan','Hamadan','Hormozgan','Ilam','Kerman','Kermanshah','Khorasan','Khuzestan','Kohgiluyeh va Buyer Ahmad','Kordestan','Lorestan','Markazi','Mazandaran','Qazvin','Qom','Semnan','Sistan va Baluchestan','Tehran','Yazd','Zanjan'] },
    { name: 'Iraq', cities: ['--Select Your State--','Al Anbar','Al Basrah','Al Muthanna','Al Qadisiyah','An Najaf','Arbil','As Sulaymaniyah','At Ta"mim','Babil','Baghdad','Dahuk','Dhi Qar','Diyala','Karbala','Maysan','Ninawa','Salah ad Din','Wasit'] },
    { name: 'Ireland', cities: ['--Select Your State--','Carlow','Cavan','Clare','Cork','Donegal','Dublin','Galway','Kerry','Kildare','Kilkenny','Laois','Leitrim','Limerick','Longford','Louth','Mayo','Meath','Monaghan','Offaly','Roscommon','Sligo','Tipperary','Waterford','Westmeath','Wexford','Wicklow'] },
    { name: 'Ireland, Northern', cities: ['--Select Your State--','Antrim','Ards','Armagh','Ballymena','Ballymoney','Banbridge','Belfast','Carrickfergus','Castlereagh','Coleraine','Cookstown','Craigavon','Derry','Down','Dungannon','Fermanagh','Larne','Limavady','Lisburn','Magherafelt','Moyle','Newry and Mourne','Newtownabbey','North Down','Omagh','Strabane'] },
    { name: 'Israel', cities: ['--Select Your State--','Central','Haifa','Jerusalem','Northern','Southern','Tel Aviv'] },
    { name: 'Italy', cities: ['--Select Your State--','Abruzzo','Basilicata','Calabria','Campania','Emilia-Romagna','Friuli-Venezia Giulia','Lazio','Liguria','Lombardia','Marche','Molise','Piemonte','Puglia','Sardegna','Sicilia','Toscana','Trentino-Alto Adige','Umbria','Valle d"Aosta','Veneto'] },
    { name: 'Jamaica', cities: ['--Select Your State--','Clarendon','Hanover','Kingston','Manchester','Portland','Saint Andrew','Saint Ann','Saint Catherine','Saint Elizabeth','Saint James','Saint Mary','Saint Thomas','Trelawny','Westmoreland'] },
    { name: 'Jan Mayen', cities: ['--Select Your State--','Jan Mayen'] },
    { name: 'Japan', cities: ['--Select Your State--','Aichi','Akita','Aomori','Chiba','Ehime','Fukui','Fukuoka','Fukushima','Gifu','Gumma','Hiroshima','Hokkaido','Hyogo','Ibaraki','Ishikawa','Iwate','Kagawa','Kagoshima','Kanagawa','Kochi','Kumamoto','Kyoto','Mie','Miyagi','Miyazaki','Nagano','Nagasaki','Nara','Niigata','Oita','Okayama','Okinawa','Osaka','Saga','Saitama','Shiga','Shimane','Shizuoka','Tochigi','Tokushima','Tokyo','Tottori','Toyama','Wakayama','Yamagata','Yamaguchi','Yamanashi'] },
    { name: 'Jarvis Island', cities: ['--Select Your State--','Jarvis Island'] },
    { name: 'Jersey', cities: ['--Select Your State--','Jersey'] },
    { name: 'Johnston Atoll', cities: ['--Select Your State--','Johnston Atoll'] },
    { name: 'Jordan', cities: ['--Select Your State--','Amman','Ajlun','Al "Aqabah','Al Balqa"','Al Karak','Al Mafraq','At Tafilah','Az Zarqa"','Irbid','Jarash','Ma"an','Madaba'] },
    { name: 'Juan de Nova Island', cities: ['--Select Your State--','Juan de Nova Island'] },
    { name: 'Kazakhstan', cities: ['--Select Your State--','Almaty','Aqmola','Aqtobe','Astana','Atyrau','Batys Qazaqstan','Bayqongyr','Mangghystau','Ongtustik Qazaqstan','Pavlodar','Qaraghandy','Qostanay','Qyzylorda','Shyghys Qazaqstan','Soltustik Qazaqstan','Zhambyl'] },
    { name: 'Kenya', cities: ['--Select Your State--','Central','Coast','Eastern','Nairobi Area','North Eastern','Nyanza','Rift Valley','Western'] },
    { name: 'Kiribati', cities: ['--Select Your State--','Abaiang','Abemama','Aranuka','Arorae','Banaba','Banaba','Beru','Butaritari','Central Gilberts','Gilbert Islands','Kanton','Kiritimati','Kuria','Line Islands','Line Islands','Maiana','Makin','Marakei','Nikunau','Nonouti','Northern Gilberts','Onotoa','Phoenix Islands','Southern Gilberts','Tabiteuea','Tabuaeran','Tamana','Tarawa','Tarawa','Teraina'] },
    { name: 'Korea, North', cities: ['--Select Your State--','Chagang-do (Chagang Province)','Hamgyong-bukto (North Hamgyong Province)','Hamgyong-namdo (South Hamgyong Province)','Hwanghae-bukto (North Hwanghae Province)','Hwanghae-namdo (South Hwanghae Province)','Kaesong-si (Kaesong City)','Kangwon-do (Kangwon Province)','Namp"o-si (Namp"o City)','P"yongan-bukto (North P"yongan Province)','P"yongan-namdo (South P"yongan Province)','P"yongyang-si (P"yongyang City)','Yanggang-do (Yanggang Province)'] },
    { name: 'Korea, South', cities: ['--Select Your State--','Ch"ungch"ong-bukto','Ch"ungch"ong-namdo','Cheju-do','Cholla-bukto','Cholla-namdo','Inch"on-gwangyoksi','Kangwon-do','Kwangju-gwangyoksi','Kyonggi-do','Kyongsang-bukto','Kyongsang-namdo','Pusan-gwangyoksi','Soul-t"ukpyolsi','Taegu-gwangyoksi','Taejon-gwangyoksi','Ulsan-gwangyoksi'] },
    { name: 'Kuwait', cities: ['--Select Your State--','Al "Asimah','Al Ahmadi','Al Farwaniyah','Al Jahra"','Hawalli'] },
    { name: 'Kyrgyzstan', cities: ['--Select Your State--','Batken Oblasty','Bishkek Shaary','Chuy Oblasty (Bishkek)','Jalal-Abad Oblasty','Naryn Oblasty','Osh Oblasty','Talas Oblasty','Ysyk-Kol Oblasty (Karakol)'] },
    { name: 'Laos', cities: ['--Select Your State--','Attapu','Bokeo','Bolikhamxai','Champasak','Houaphan','Khammouan','Louangnamtha','Louangphabang','Oudomxai','Phongsali','Salavan','Savannakhet','Viangchan','Viangchan','Xaignabouli','Xaisomboun','Xekong','Xiangkhoang'] },
    { name: 'Latvia', cities: ['--Select Your State--','Aizkraukles Rajons','Aluksnes Rajons','Balvu Rajons','Bauskas Rajons','Cesu Rajons','Daugavpils','Daugavpils Rajons','Dobeles Rajons','Gulbenes Rajons','Jekabpils Rajons','Jelgava','Jelgavas Rajons','Jurmala','Kraslavas Rajons','Kuldigas Rajons','Leipaja','Liepajas Rajons','Limbazu Rajons','Ludzas Rajons','Madonas Rajons','Ogres Rajons','Preilu Rajons','Rezekne','Rezeknes Rajons','Riga','Rigas Rajons','Saldus Rajons','Talsu Rajons','Tukuma Rajons','Valkas Rajons','Valmieras Rajons','Ventspils','Ventspils Rajons'] },
    { name: 'Lebanon', cities: ['--Select Your State--','Beyrouth','Ech Chimal','Ej Jnoub','El Bekaa','Jabal Loubnane'] },
    { name: 'Lesotho', cities: ['--Select Your State--','Berea','Butha-Buthe','Leribe','Mafeteng','Maseru','Mohales Hoek','Mokhotlong','Qacha"s Nek','Quthing','Thaba-Tseka'] },
    { name: 'Liberia', cities: ['--Select Your State--','Bomi','Bong','Grand Bassa','Grand Cape Mount','Grand Gedeh','Grand Kru','Lofa','Margibi','Maryland','Montserrado','Nimba','River Cess','Sinoe'] },
    { name: 'Libya', cities: ['--Select Your State--','Ajdabiya','Al "Aziziyah','Al Fatih','Al Jabal al Akhdar','Al Jufrah','Al Khums','Al Kufrah','An Nuqat al Khams','Ash Shati"','Awbari','Az Zawiyah','Banghazi','Darnah','Ghadamis','Gharyan','Misratah','Murzuq','Sabha','Sawfajjin','Surt','Tarabulus','Tarhunah','Tubruq','Yafran','Zlitan'] },
    { name: 'Liechtenstein', cities: ['--Select Your State--','Balzers','Eschen','Gamprin','Mauren','Planken','Ruggell','Schaan','Schellenberg','Triesen','Triesenberg','Vaduz'] },
    { name: 'Lithuania', cities: ['--Select Your State--','Akmenes Rajonas','Alytaus Rajonas','Alytus','Anyksciu Rajonas','Birstonas','Birzu Rajonas','Druskininkai','Ignalinos Rajonas','Jonavos Rajonas','Joniskio Rajonas','Jurbarko Rajonas','Kaisiadoriu Rajonas','Kaunas','Kauno Rajonas','Kedainiu Rajonas','Kelmes Rajonas','Klaipeda','Klaipedos Rajonas','Kretingos Rajonas','Kupiskio Rajonas','Lazdiju Rajonas','Marijampole','Marijampoles Rajonas','Mazeikiu Rajonas','Moletu Rajonas','Neringa Pakruojo Rajonas','Palanga','Panevezio Rajonas','Panevezys','Pasvalio Rajonas','Plunges Rajonas','Prienu Rajonas','Radviliskio Rajonas','Raseiniu Rajonas','Rokiskio Rajonas','Sakiu Rajonas','Salcininku Rajonas','Siauliai','Siauliu Rajonas','Silales Rajonas','Silutes Rajonas','Sirvintu Rajonas','Skuodo Rajonas','Svencioniu Rajonas','Taurages Rajonas','Telsiu Rajonas','Traku Rajonas','Ukmerges Rajonas','Utenos Rajonas','Varenos Rajonas','Vilkaviskio Rajonas','Vilniaus Rajonas','Vilnius','Zarasu Rajonas'] },
    { name: 'Luxembourg', cities: ['--Select Your State--','Diekirch','Grevenmacher','Luxembourg'] },
    { name: 'Macau', cities: ['--Select Your State--','Macau'] },
    { name: 'Macedonia, Former Yugoslav Republic of', cities: ['--Select Your State--','Aracinovo','Bac','Belcista','Berovo','Bistrica','Bitola','Blatec','Bogdanci','Bogomila','Bogovinje','Bosilovo','Brvenica','Cair (Skopje)','Capari','Caska','Cegrane','Centar (Skopje)','Centar Zupa','Cesinovo','Cucer-Sandevo','Debar','Delcevo','Delogozdi','Demir Hisar','Demir Kapija','Dobrusevo','Dolna Banjica','Dolneni','Dorce Petrov (Skopje)','Drugovo','Dzepciste','Gazi Baba (Skopje)','Gevgelija','Gostivar','Gradsko','Ilinden','Izvor','Jegunovce','Kamenjane','Karbinci','Karpos (Skopje)','Kavadarci','Kicevo','Kisela Voda (Skopje)','Klecevce','Kocani','Konce','Kondovo','Konopiste','Kosel','Kratovo','Kriva Palanka','Krivogastani','Krusevo','Kuklis','Kukurecani','Kumanovo','Labunista','Lipkovo','Lozovo','Lukovo','Makedonska Kamenica','Makedonski Brod','Mavrovi Anovi','Meseista','Miravci','Mogila','Murtino','Negotino','Negotino-Poloska','Novaci','Novo Selo','Oblesevo','Ohrid','Orasac','Orizari','Oslomej','Pehcevo','Petrovec','Plasnia','Podares','Prilep','Probistip','Radovis','Rankovce','Resen','Rosoman','Rostusa','Samokov','Saraj','Sipkovica','Sopiste','Sopotnika','Srbinovo','Star Dojran','Staravina','Staro Nagoricane','Stip','Struga','Strumica','Studenicani','Suto Orizari (Skopje)','Sveti Nikole','Tearce','Tetovo','Topolcani','Valandovo','Vasilevo','Veles','Velesta','Vevcani','Vinica','Vitoliste','Vranestica','Vrapciste','Vratnica','Vrutok','Zajas','Zelenikovo','Zileno','Zitose','Zletovo','Zrnovci'] },
    { name: 'Madagascar', cities: ['--Select Your State--','Antananarivo','Antsiranana','Fianarantsoa','Mahajanga','Toamasina','Toliara'] },
    { name: 'Malawi', cities: ['--Select Your State--','Balaka','Blantyre','Chikwawa','Chiradzulu','Chitipa','Dedza','Dowa','Karonga','Kasungu','Likoma','Lilongwe','Machinga (Kasupe)','Mangochi','Mchinji','Mulanje','Mwanza','Mzimba','Nkhata Bay','Nkhotakota','Nsanje','Ntcheu','Ntchisi','Phalombe','Rumphi','Salima','Thyolo','Zomba'] },
    { name: 'Malaysia', cities: ['--Select Your State--','Johor','Kedah','Kelantan','Labuan','Melaka','Negeri Sembilan','Pahang','Perak','Perlis','Pulau Pinang','Sabah','Sarawak','Selangor','Terengganu','Wilayah Persekutuan'] },
    { name: 'Maldives', cities: ['--Select Your State--','Alifu','Baa','Dhaalu','Faafu','Gaafu Alifu','Gaafu Dhaalu','Gnaviyani','Haa Alifu','Haa Dhaalu','Kaafu','Laamu','Lhaviyani','Maale','Meemu','Noonu','Raa','Seenu','Shaviyani','Thaa','Vaavu'] },
    { name: 'Mali', cities: ['--Select Your State--','Gao','Kayes','Kidal','Koulikoro','Mopti','Segou','Sikasso','Tombouctou'] },
    { name: 'Malta', cities: ['--Select Your State--','Valletta'] },
    { name: 'Man, Isle of', cities: ['--Select Your State--','Man',' Isle of'] },
    { name: 'Marshall Islands', cities: ['--Select Your State--','Ailinginae','Ailinglaplap','Ailuk','Arno','Aur','Bikar','Bikini','Bokak','Ebon','Enewetak','Erikub','Jabat','Jaluit','Jemo','Kili','Kwajalein','Lae','Lib','Likiep','Majuro','Maloelap','Mejit','Mili','Namorik','Namu','Rongelap','Rongrik','Toke','Ujae','Ujelang','Utirik','Wotho','Wotje'] },
    { name: 'Martinique', cities: ['--Select Your State--','Martinique'] },
    { name: 'Mauritania', cities: ['--Select Your State--','Adrar','Assaba','Brakna','Dakhlet Nouadhibou','Gorgol','Guidimaka','Hodh Ech Chargui','Hodh El Gharbi','Inchiri','Nouakchott','Tagant','Tiris Zemmour','Trarza'] },
    { name: 'Mauritius', cities: ['--Select Your State--','Agalega Islands','Black River','Cargados Carajos Shoals','Flacq','Grand Port','Moka','Pamplemousses','Plaines Wilhems','Port Louis','Riviere du Rempart','Rodrigues','Savanne'] },
    { name: 'Mayotte', cities: ['--Select Your State--','Mayotte'] },
    { name: 'Mexico', cities: ['--Select Your State--','Aguascalientes','Baja California','Baja California Sur','Campeche','Chiapas','Chihuahua','Coahuila de Zaragoza','Colima','Distrito Federal','Durango','Guanajuato','Guerrero','Hidalgo','Jalisco','Mexico','Michoacan de Ocampo','Morelos','Nayarit','Nuevo Leon','Oaxaca','Puebla','Queretaro de Arteaga','Quintana Roo','San Luis Potosi','Sinaloa','Sonora','Tabasco','Tamaulipas','Tlaxcala','Veracruz-Llave','Yucatan','Zacatecas'] },
    { name: 'Micronesia, Federated States of', cities: ['--Select Your State--','Chuuk (Truk)','Kosrae','Pohnpei','Yap'] },
    { name: 'Midway Islands', cities: ['--Select Your State--','Midway Islands'] },
    { name: 'Moldova', cities: ['--Select Your State--','Balti','Cahul','Chisinau','Chisinau','Dubasari','Edinet','Gagauzia','Lapusna','Orhei','Soroca','Tighina','Ungheni'] },
    { name: 'Monaco', cities: ['--Select Your State--','Fontvieille','La Condamine','Monaco-Ville','Monte-Carlo'] },
    { name: 'Mongolia', cities: ['--Select Your State--','Arhangay','Bayan-Olgiy','Bayanhongor','Bulgan','Darhan','Dornod','Dornogovi','Dundgovi','Dzavhan','Erdenet','Govi-Altay','Hentiy','Hovd','Hovsgol','Omnogovi','Ovorhangay','Selenge','Suhbaatar','Tov','Ulaanbaatar','Uvs'] },
    { name: 'Montserrat', cities: ['--Select Your State--','Saint Anthony','Saint Georges','Saint Peter"s'] },
    { name: 'Morocco', cities: ['--Select Your State--','Agadir','Al Hoceima','Azilal','Ben Slimane','Beni Mellal','Boulemane','Casablanca','Chaouen','El Jadida','El Kelaa des Srarhna','Er Rachidia','Essaouira','Fes','Figuig','Guelmim','Ifrane','Kenitra','Khemisset','Khenifra','Khouribga','Laayoune','Larache','Marrakech','Meknes','Nador','Ouarzazate','Oujda','Rabat-Sale','Safi','Settat','Sidi Kacem','Tan-Tan','Tanger','Taounate','Taroudannt','Tata','Taza','Tetouan','Tiznit'] },
    { name: 'Mozambique', cities: ['--Select Your State--','Cabo Delgado','Gaza','Inhambane','Manica','Maputo','Nampula','Niassa','Sofala','Tete','Zambezia'] },
    { name: 'Namibia', cities: ['--Select Your State--','Caprivi','Erongo','Hardap','Karas','Khomas','Kunene','Ohangwena','Okavango','Omaheke','Omusati','Oshana','Oshikoto','Otjozondjupa'] },
    { name: 'Nauru', cities: ['--Select Your State--','Aiwo','Anabar','Anetan','Anibare','Baiti','Boe','Buada','Denigomodu','Ewa','Ijuw','Meneng','Nibok','Uaboe','Yaren'] },
    { name: 'Nepal', cities: ['--Select Your State--','Bagmati','Bheri','Dhawalagiri','Gandaki','Janakpur','Karnali','Kosi','Lumbini','Mahakali','Mechi','Narayani','Rapti','Sagarmatha','Seti'] },
    { name: 'Netherlands', cities: ['--Select Your State--','Drenthe','Flevoland','Friesland','Gelderland','Groningen','Limburg','Noord-Brabant','Noord-Holland','Overijssel','Utrecht','Zeeland','Zuid-Holland'] },
    { name: 'Netherlands Antilles', cities: ['--Select Your State--','Netherlands Antilles'] },
    { name: 'New Caledonia', cities: ['--Select Your State--','Iles Loyaute','Nord','Sud'] },
    { name: 'New Zealand', cities: ['--Select Your State--','Akaroa','Amuri','Ashburton','Bay of Islands','Bruce','Buller','Chatham Islands','Cheviot','Clifton','Clutha','Cook','Dannevirke','Egmont','Eketahuna','Ellesmere','Eltham','Eyre','Featherston','Franklin','Golden Bay','Great Barrier Island','Grey','Hauraki Plains','Hawera','Hawke"s Bay','Heathcote','Hikurangi','Hobson','Hokianga','Horowhenua','Hurunui','Hutt','Inangahua','Inglewood','Kaikoura','Kairanga','Kiwitea','Lake','Mackenzie','Malvern','Manaia','Manawatu','Mangonui','Maniototo','Marlborough','Masterton','Matamata','Mount Herbert','Ohinemuri','Opotiki','Oroua','Otamatea','Otorohanga','Oxford','Pahiatua','Paparua','Patea','Piako','Pohangina','Raglan','Rangiora','Rangitikei','Rodney','Rotorua','Runanga','Saint Kilda','Silverpeaks','Southland','Stewart Island','Stratford','Strathallan','Taranaki','Taumarunui','Taupo','Tauranga','Thames-Coromandel','Tuapeka','Vincent','Waiapu','Waiheke','Waihemo','Waikato','Waikohu','Waimairi','Waimarino','Waimate','Waimate West','Waimea','Waipa','Waipawa','Waipukurau','Wairarapa South','Wairewa','Wairoa','Waitaki','Waitomo','Waitotara','Wallace','Wanganui','Waverley','Westland','Whakatane','Whangarei','Whangaroa','Woodville'] },
    { name: 'Nicaragua', cities: ['--Select Your State--','Atlantico Norte','Atlantico Sur','Boaco','Carazo','Chinandega','Chontales','Esteli','Granada','Jinotega','Leon','Madriz','Managua','Masaya','Matagalpa','Nueva Segovia','Rio San Juan','Rivas'] },
    { name: 'Niger', cities: ['--Select Your State--','Agadez','Diffa','Dosso','Maradi','Niamey','Tahoua','Tillaberi','Zinder'] },
    { name: 'Nigeria', cities: ['--Select Your State--','Abia','Abuja Federal Capital Territory','Adamawa','Akwa Ibom','Anambra','Bauchi','Bayelsa','Benue','Borno','Cross River','Delta','Ebonyi','Edo','Ekiti','Enugu','Gombe','Imo','Jigawa','Kaduna','Kano','Katsina','Kebbi','Kogi','Kwara','Lagos','Nassarawa','Niger','Ogun','Ondo','Osun','Oyo','Plateau','Rivers','Sokoto','Taraba','Yobe','Zamfara'] },
    { name: 'Niue', cities: ['--Select Your State--','Niue'] },
    { name: 'Norfolk Island', cities: ['--Select Your State--','Norfolk Island'] },
    { name: 'Northern Mariana Islands', cities: ['--Select Your State--','Northern Islands','Rota','Saipan','Tinian'] },
    { name: 'Norway', cities: ['--Select Your State--','Akershus','Aust-Agder','Buskerud','Finnmark','Hedmark','Hordaland','More og Romsdal','Nord-Trondelag','Nordland','Oppland','Oslo','Ostfold','Rogaland','Sogn og Fjordane','Sor-Trondelag','Telemark','Troms','Vest-Agder','Vestfold'] },
    { name: 'Oman', cities: ['--Select Your State--','Ad Dakhiliyah','Al Batinah','Al Wusta','Ash Sharqiyah','Az Zahirah','Masqat','Musandam','Zufar'] },
    { name: 'Pakistan', cities: ['--Select Your State--','Balochistan','Federally Administered Tribal Areas','Islamabad Capital Territory','North-West Frontier Province','Punjab','Sindh'] },
    { name: 'Palau', cities: ['--Select Your State--','Aimeliik','Airai','Angaur','Hatobohei','Kayangel','Koror','Melekeok','Ngaraard','Ngarchelong','Ngardmau','Ngatpang','Ngchesar','Ngeremlengui','Ngiwal','Palau Island','Peleliu','Sonsoral','Tobi'] },
    { name: 'Panama', cities: ['--Select Your State--','Bocas del Toro','Chiriqui','Cocle','Colon','Darien','Herrera','Los Santos','Panama','San Blas','Veraguas'] },
    { name: 'Papua New Guinea', cities: ['--Select Your State--','Bougainville','Central','Chimbu','East New Britain','East Sepik','Eastern Highlands','Enga','Gulf','Madang','Manus','Milne Bay','Morobe','National Capital','New Ireland','Northern','Sandaun','Southern Highlands','West New Britain','Western','Western Highlands'] },
    { name: 'Paraguay', cities: ['--Select Your State--','Alto Paraguay','Alto Parana','Amambay','Asuncion (city)','Boqueron','Caaguazu','Caazapa','Canindeyu','Central','Concepcion','Cordillera','Guaira','Itapua','Misiones','Neembucu','Paraguari','Presidente Hayes','San Pedro'] },
    { name: 'Peru', cities: ['--Select Your State--','Amazonas','Ancash','Apurimac','Arequipa','Ayacucho','Cajamarca','Callao','Cusco','Huancavelica','Huanuco','Ica','Junin','La Libertad','Lambayeque','Lima','Loreto','Madre de Dios','Moquegua','Pasco','Piura','Puno','San Martin','Tacna','Tumbes','Ucayali'] },
    { name: 'Philippines', cities: ['--Select Your State--','Abra','Agusan del Norte','Agusan del Sur','Aklan','Albay','Angeles','Antique','Aurora','Bacolod','Bago','Baguio','Bais','Basilan','Basilan City','Bataan','Batanes','Batangas','Batangas City','Benguet','Bohol','Bukidnon','Bulacan','Butuan','Cabanatuan','Cadiz','Cagayan','Cagayan de Oro','Calbayog','Caloocan','Camarines Norte','Camarines Sur','Camiguin','Canlaon','Capiz','Catanduanes','Cavite','Cavite City','Cebu','Cebu City','Cotabato','Dagupan','Danao','Dapitan','Davao City Davao','Davao del Sur','Davao Oriental','Dipolog','Dumaguete','Eastern Samar','General Santos','Gingoog','Ifugao','Iligan','Ilocos Norte','Ilocos Sur','Iloilo','Iloilo City','Iriga','Isabela','Kalinga-Apayao','La Carlota','La Union','Laguna','Lanao del Norte','Lanao del Sur','Laoag','Lapu-Lapu','Legaspi','Leyte','Lipa','Lucena','Maguindanao','Mandaue','Manila','Marawi','Marinduque','Masbate','Mindoro Occidental','Mindoro Oriental','Misamis Occidental','Misamis Oriental','Mountain','Naga','Negros Occidental','Negros Oriental','North Cotabato','Northern Samar','Nueva Ecija','Nueva Vizcaya','Olongapo','Ormoc','Oroquieta','Ozamis','Pagadian','Palawan','Palayan','Pampanga','Pangasinan','Pasay','Puerto Princesa','Quezon','Quezon City','Quirino','Rizal','Romblon','Roxas','Samar','San Carlos (in Negros Occidental)','San Carlos (in Pangasinan)','San Jose','San Pablo','Silay','Siquijor','Sorsogon','South Cotabato','Southern Leyte','Sultan Kudarat','Sulu','Surigao','Surigao del Norte','Surigao del Sur','Tacloban','Tagaytay','Tagbilaran','Tangub','Tarlac','Tawitawi','Toledo','Trece Martires','Zambales','Zamboanga','Zamboanga del Norte','Zamboanga del Sur'] },
    { name: 'Pitcaim Islands', cities: ['--Select Your State--','Pitcaim Islands'] },
    { name: 'Poland', cities: ['--Select Your State--','Dolnoslaskie','Kujawsko-Pomorskie','Lodzkie','Lubelskie','Lubuskie','Malopolskie','Mazowieckie','Opolskie','Podkarpackie','Podlaskie','Pomorskie','Slaskie','Swietokrzyskie','Warminsko-Mazurskie','Wielkopolskie','Zachodniopomorskie'] },
    { name: 'Portugal', cities: ['--Select Your State--','Acores (Azores)','Aveiro','Beja','Braga','Braganca','Castelo Branco','Coimbra','Evora','Faro','Guarda','Leiria','Lisboa','Madeira','Portalegre','Porto','Santarem','Setubal','Viana do Castelo','Vila Real','Viseu'] },
    { name: 'Puerto Rico', cities: ['--Select Your State--','Adjuntas','Aguada','Aguadilla','Aguas Buenas','Aibonito','Anasco','Arecibo','Arroyo','Barceloneta','Barranquitas','Bayamon','Cabo Rojo','Caguas','Camuy','Canovanas','Carolina','Catano','Cayey','Ceiba','Ciales','Cidra','Coamo','Comerio','Corozal','Culebra','Dorado','Fajardo','Florida','Guanica','Guayama','Guayanilla','Guaynabo','Gurabo','Hatillo','Hormigueros','Humacao','Isabela','Jayuya','Juana Diaz','Juncos','Lajas','Lares','Las Marias','Las Piedras','Loiza','Luquillo','Manati','Maricao','Maunabo','Mayaguez','Moca','Morovis','Naguabo','Naranjito','Orocovis','Patillas','Penuelas','Ponce','Quebradillas','Rincon','Rio Grande','Sabana Grande','Salinas','San German','San Juan','San Lorenzo','San Sebastian','Santa Isabel','Toa Alta','Toa Baja','Trujillo Alto','Utuado','Vega Alta','Vega Baja','Vieques','Villalba','Yabucoa','Yauco'] },
    { name: 'Qatar', cities: ['--Select Your State--','Ad Dawhah','Al Ghuwayriyah','Al Jumayliyah','Al Khawr','Al Wakrah','Ar Rayyan','Jarayan al Batinah','Madinat ash Shamal','Umm Salal'] },
    { name: 'Reunion', cities: ['--Select Your State--','Reunion'] },
    { name: 'Romainia', cities: ['--Select Your State--','Alba','Arad','Arges','Bacau','Bihor','Bistrita-Nasaud','Botosani','Braila','Brasov','Bucuresti','Buzau','Calarasi','Caras-Severin','Cluj','Constanta','Covasna','Dimbovita','Dolj','Galati','Giurgiu','Gorj','Harghita','Hunedoara','Ialomita','Iasi','Maramures','Mehedinti','Mures','Neamt','Olt','Prahova','Salaj','Satu Mare','Sibiu','Suceava','Teleorman','Timis','Tulcea','Vaslui','Vilcea','Vrancea'] },
    { name: 'Russia', cities: ['--Select Your State--','Adygeya (Maykop)','Aginskiy Buryatskiy (Aginskoye)','Altay (Gorno-Altaysk)','Altayskiy (Barnaul)','Amurskaya (Blagoveshchensk)','Arkhangel"skaya','Astrakhanskaya','Bashkortostan (Ufa)','Belgorodskaya','Bryanskaya','Buryatiya (Ulan-Ude)','Chechnya (Groznyy)','Chelyabinskaya','Chitinskaya','Chukotskiy (Anadyr")','Chuvashiya (Cheboksary)','Dagestan (Makhachkala)','Evenkiyskiy (Tura)','Ingushetiya (Nazran")','Irkutskaya','Ivanovskaya','Kabardino-Balkariya (Nal"chik)','Kaliningradskaya','Kalmykiya (Elista)','Kaluzhskaya','Kamchatskaya (Petropavlovsk-Kamchatskiy)','Karachayevo-Cherkesiya (Cherkessk)','Kareliya (Petrozavodsk)','Kemerovskaya','Khabarovskiy','Khakasiya (Abakan)','Khanty-Mansiyskiy (Khanty-Mansiysk)','Kirovskaya','Komi (Syktyvkar)','Komi-Permyatskiy (Kudymkar)','Koryakskiy (Palana)','Kostromskaya','Krasnodarskiy','Krasnoyarskiy','Kurganskaya','Kurskaya','Leningradskaya','Lipetskaya','Magadanskaya','Mariy-El (Yoshkar-Ola)','Mordoviya (Saransk)','Moskovskaya','Moskva (Moscow)','Murmanskaya','Nenetskiy (Nar"yan-Mar)','Nizhegorodskaya','Novgorodskaya','Novosibirskaya','Omskaya','Orenburgskaya','Orlovskaya (Orel)','Penzenskaya','Permskaya','Primorskiy (Vladivostok)','Pskovskaya','Rostovskaya','Ryazanskaya','Sakha (Yakutsk)','Sakhalinskaya (Yuzhno-Sakhalinsk)','Samarskaya','Sankt-Peterburg (Saint Petersburg)','Saratovskaya','Severnaya Osetiya-Alaniya [North Ossetia] (Vladikavkaz)','Smolenskaya','Stavropol"skiy','Sverdlovskaya (Yekaterinburg)','Tambovskaya','Tatarstan (Kazan)','Taymyrskiy (Dudinka)','Tomskaya','Tul"skaya','Tverskaya','Tyumenskaya','Tyva (Kyzyl)','Udmurtiya (Izhevsk)','Ul"yanovskaya','Ust"-Ordynskiy Buryatskiy (Ust"-Ordynskiy)','Vladimirskaya','Volgogradskaya','Vologodskaya','Voronezhskaya','Yamalo-Nenetskiy (Salekhard)','Yaroslavskaya','Yevreyskaya'] },
    { name: 'Rwanda', cities: ['--Select Your State--','Butare','Byumba','Cyangugu','Gikongoro','Gisenyi','Gitarama','Kibungo','Kibuye','Kigali Rurale','Kigali-ville','Ruhengeri','Umutara'] },
    { name: 'Saint Helena', cities: ['--Select Your State--','Ascension','Saint Helena','Tristan da Cunha'] },
    { name: 'Saint Kitts and Nevis', cities: ['--Select Your State--','Christ Church Nichola Town','Saint Anne Sandy Point','Saint George Basseterre','Saint George Gingerland','Saint James Windward','Saint John Capisterre','Saint John Figtree','Saint Mary Cayon','Saint Paul Capisterre','Saint Paul Charlestown','Saint Peter Basseterre','Saint Thomas Lowland','Saint Thomas Middle Island','Trinity Palmetto Point'] },
    { name: 'Saint Lucia', cities: ['--Select Your State--','Anse-la-Raye','Castries','Choiseul','Dauphin','Dennery','Gros Islet','Laborie','Micoud','Praslin','Soufriere','Vieux Fort'] },
    { name: 'Saint Pierre and Miquelon', cities: ['--Select Your State--','Miquelon','Saint Pierre'] },
    { name: 'Saint Vincent and the Grenadines', cities: ['--Select Your State--','Charlotte','Grenadines','Saint Andrew','Saint David','Saint George','Saint Patrick'] },
    { name: 'Samoa', cities: ['--Select Your State--','A"ana','Aiga-i-le-Tai','Atua','Fa"asaleleaga','Gaga"emauga','Gagaifomauga','Palauli','Satupa"itea','Tuamasaga','Va"a-o-Fonoti','Vaisigano'] },
    { name: 'San Marino', cities: ['--Select Your State--','Acquaviva','Borgo Maggiore','Chiesanuova','Domagnano','Faetano','Fiorentino','Monte Giardino','San Marino','Serravalle'] },
    { name: 'Sao Tome and Principe', cities: ['--Select Your State--','Principe','Sao Tome'] },
    { name: 'Saudi Arabia', cities: ['--Select Your State--','Asir','Al Bahah','Al Hudud ash Shamaliyah','Al Jawf','Al Madinah','Al Qasim','Ar Riyad','Ash Sharqiyah (Eastern Province)','Ha"il','Jizan','Makkah','Najran','Tabuk'] },
    { name: 'Scotland', cities: ['--Select Your State--','Aberdeen City','Aberdeenshire','Angus','Argyll and Bute','City of Edinburgh','Clackmannanshire','Dumfries and Galloway','Dundee City','East Ayrshire','East Dunbartonshire','East Lothian','East Renfrewshire','Eilean Siar (Western Isles)','Falkirk','Fife','Glasgow City','Highland','Inverclyde','Midlothian','Moray','North Ayrshire','North Lanarkshire','Orkney Islands','Perth and Kinross','Renfrewshire','Shetland Islands','South Ayrshire','South Lanarkshire','Stirling','The Scottish Borders','West Dunbartonshire','West Lothian'] },
    { name: 'Senegal', cities: ['--Select Your State--','Dakar','Diourbel','Fatick','Kaolack','Kolda','Louga','Saint-Louis','Tambacounda','Thies','Ziguinchor'] },
    { name: 'Seychelles', cities: ['--Select Your State--','Anse aux Pins','Anse Boileau','Anse Etoile','Anse Louis','Anse Royale','Baie Lazare','Baie Sainte Anne','Beau Vallon','Bel Air','Bel Ombre','Cascade','Glacis','Grand" Anse (on Mahe)','Grand" Anse (on Praslin)','La Digue','La Riviere Anglaise','Mont Buxton','Mont Fleuri','Plaisance','Pointe La Rue','Port Glaud','Saint Louis','Takamaka'] },
    { name: 'ChiSierra Leone', cities: ['--Select Your State--','Eastern','Northern','Southern','Western'] },
    { name: 'Singapore', cities: ['--Select Your State--','Singapore'] },
    { name: 'Slovakia', cities: ['--Select Your State--','Banskobystricky','Bratislavsky','Kosicky','Nitriansky','Presovsky','Trenciansky','Trnavsky','Zilinsky'] },
    { name: 'Slovenia', cities: ['--Select Your State--','Ajdovscina','Beltinci','Bled','Bohinj','Borovnica','Bovec','Brda','Brezice','Brezovica','Cankova-Tisina','Celje','Cerklje na Gorenjskem','Cerknica','Cerkno','Crensovci','Crna na Koroskem','Crnomelj','Destrnik-Trnovska Vas','Divaca','Dobrepolje','Dobrova-Horjul-Polhov Gradec','Dol pri Ljubljani','Domzale','Dornava','Dravograd','Duplek','Gorenja Vas-Poljane','Gorisnica','Gornja Radgona','Gornji Grad','Gornji Petrovci','Grosuplje','Hodos Salovci','Hrastnik','Hrpelje-Kozina','Idrija','Ig','Ilirska Bistrica','Ivancna Gorica','Izola','Jesenice','Jursinci','Kamnik','Kanal','Kidricevo','Kobarid','Kobilje','Kocevje','Komen','Koper','Kozje','Kranj','Kranjska Gora','Krsko','Kungota','Kuzma','Lasko','Lenart','Lendava','Litija','Ljubljana','Ljubno','Ljutomer','Logatec','Loska Dolina','Loski Potok','Luce','Lukovica','Majsperk','Maribor','Medvode','Menges','Metlika','Mezica','Miren-Kostanjevica','Mislinja','Moravce','Moravske Toplice','Mozirje','Murska Sobota','Muta','Naklo','Nazarje','Nova Gorica','Novo Mesto','Odranci','Ormoz','Osilnica','Pesnica','Piran','Pivka','Podcetrtek','Podvelka-Ribnica','Postojna','Preddvor','Ptuj','Puconci','Race-Fram','Radece','Radenci','Radlje ob Dravi','Radovljica','Ravne-Prevalje','Ribnica','Rogasevci','Rogaska Slatina','Rogatec','Ruse','Semic','Sencur','Sentilj','Sentjernej','Sentjur pri Celju','Sevnica','Sezana','Skocjan','Skofja Loka','Skofljica','Slovenj Gradec','Slovenska Bistrica','Slovenske Konjice','Smarje pri Jelsah','Smartno ob Paki','Sostanj','Starse','Store','Sveti Jurij','Tolmin','Trbovlje','Trebnje','Trzic','Turnisce','Velenje','Velike Lasce','Videm','Vipava','Vitanje','Vodice','Vojnik','Vrhnika','Vuzenica','Zagorje ob Savi','Zalec','Zavrc','Zelezniki','Ziri','Zrece'] },
    { name: 'Solomon Islands', cities: ['--Select Your State--','Bellona','Central','Choiseul (Lauru)','Guadalcanal','Honiara','Isabel','Makira','Malaita','Rennell','Temotu','Western'] },
    { name: 'Somalia', cities: ['--Select Your State--','Awdal','Bakool','Banaadir','Bari','Bay','Galguduud','Gedo','Hiiraan','Jubbada Dhexe','Jubbada Hoose','Mudug','Nugaal','Sanaag','Shabeellaha Dhexe','Shabeellaha Hoose','Sool','Togdheer','Woqooyi Galbeed'] },
    { name: 'South Africa', cities: ['--Select Your State--','Eastern Cape','Free State','Gauteng','KwaZulu-Natal','Mpumalanga','North-West','Northern Cape','Northern Province','Western Cape'] },
    { name: 'South Georgia and South Sandwich Islands', cities: ['--Select Your State--','Bird Island','Bristol Island','Clerke Rocks','Montagu Island','Saunders Island','South Georgia','Southern Thule','Traversay Islands'] },
    { name: 'Spain', cities: ['--Select Your State--','Andalucia','Aragon','Asturias','Baleares (Balearic Islands)','Canarias (Canary Islands)','Cantabria','Castilla y Leon','Castilla-La Mancha','Cataluna','Ceuta','Communidad Valencian','Extremadura','Galicia','Islas Chafarinas','La Rioja','Madrid','Melilla','Murcia','Navarra','Pais Vasco (Basque Country)','Penon de Alhucemas','Penon de Velez de la Gomera'] },
    { name: 'Spratly Islands', cities: ['--Select Your State--','Spratly Islands'] },
    { name: 'Sri Lanka', cities: ['--Select Your State--','Central','Eastern','North Central','North Eastern','North Western','Northern','Sabaragamuwa','Southern','Uva','Western'] },
    { name: 'Sudan', cities: ['--Select Your State--','A"ali an Nil','Al Bahr al Ahmar','Al Buhayrat','Al Jazirah','Al Khartum','Al Qadarif','Al Wahdah','An Nil al Abyad','An Nil al Azraq','Ash Shamaliyah','Bahr al Jabal','Gharb al Istiwa"iyah','Gharb Bahr al Ghazal','Gharb Darfur','Gharb Kurdufan','Janub Darfur','Janub Kurdufan','Junqali','Kassala','Nahr an Nil','Shamal Bahr al Ghazal','Shamal Darfur','Shamal Kurdufan','Sharq al Istiwa"iyah','Sinnar','Warab'] },
    { name: 'Suriname', cities: ['--Select Your State--','Brokopondo','Commewijne','Coronie','Marowijne','Nickerie','Para','Paramaribo','Saramacca','Sipaliwini','Wanica'] },
    { name: 'Svalbard', cities: ['--Select Your State--','Barentsoya','Bjornoya','Edgeoya','Hopen','Kvitoya','Nordaustandet','Prins Karls Forland','Spitsbergen'] },
    { name: 'Swaziland', cities: ['--Select Your State--','Hhohho','Lubombo','Manzini','Shiselweni'] },
    { name: 'Sweden', cities: ['--Select Your State--','Blekinge','Dalarnas','Gavleborgs','Gotlands','Hallands','Jamtlands','Jonkopings','Kalmar','Kronobergs','Norrbottens','Orebro','Ostergotlands','Skane','Sodermanlands','Stockholms','Uppsala','Varmlands','Vasterbottens','Vasternorrlands','Vastmanlands','Vastra Gotalands'] },
    { name: 'Switzerland', cities: ['--Select Your State--','Aargau','Ausser-Rhoden','Basel-Landschaft','Basel-Stadt','Bern','Fribourg','Geneve','Glarus','Graubunden','Inner-Rhoden','Jura','Luzern','Neuchatel','Nidwalden','Obwalden','Sankt Gallen','Schaffhausen','Schwyz','Solothurn','Thurgau','Ticino','Uri','Valais','Vaud','Zug','Zurich'] },
    { name: 'Syria', cities: ['--Select Your State--','Al Hasakah','Al Ladhiqiyah','Al Qunaytirah','Ar Raqqah','As Suwayda','Dar"a','Dayr az Zawr','Dimashq','Halab','Hamah','Hims','Idlib','Rif Dimashq','Tartus'] },
    { name: 'Taiwan', cities: ['--Select Your State--','Chang-hua','Chi-lung','Chia-i','Chia-i','Chung-hsing-hsin-ts"un','Hsin-chu','Hsin-chu','Hua-lien','I-lan','Kao-hsiung','Kao-hsiung','Miao-li','Nan-t"ou','P"eng-hu','P"ing-tung','T"ai-chung','T"ai-chung','T"ai-nan','T"ai-nan','T"ai-pei','T"ai-pei','T"ai-tung','T"ao-yuan','Yun-lin'] },
    { name: 'Tajikistan', cities: ['--Select Your State--','Viloyati Khatlon','Viloyati Leninobod','Viloyati Mukhtori Kuhistoni Badakhshon'] },
    { name: 'Tanzania', cities: ['--Select Your State--','Arusha','Dar es Salaam','Dodoma','Iringa','Kagera','Kigoma','Kilimanjaro','Lindi','Mara','Mbeya','Morogoro','Mtwara','Mwanza','Pemba North','Pemba South','Pwani','Rukwa','Ruvuma','Shinyanga','Singida','Tabora','Tanga','Zanzibar Central/South','Zanzibar North','Zanzibar Urban/West'] },
    { name: 'Thailand', cities: ['--Select Your State--','Amnat Charoen','Ang Thong','Buriram','Chachoengsao','Chai Nat','Chaiyaphum','Chanthaburi','Chiang Mai','Chiang Rai','Chon Buri','Chumphon','Kalasin','Kamphaeng Phet','Kanchanaburi','Khon Kaen','Krabi','Krung Thep Mahanakhon (Bangkok)','Lampang','Lamphun','Loei','Lop Buri','Mae Hong Son','Maha Sarakham','Mukdahan','Nakhon Nayok','Nakhon Pathom','Nakhon Phanom','Nakhon Ratchasima','Nakhon Sawan','Nakhon Si Thammarat','Nan','Narathiwat','Nong Bua Lamphu','Nong Khai','Nonthaburi','Pathum Thani','Pattani','Phangnga','Phatthalung','Phayao','Phetchabun','Phetchaburi','Phichit','Phitsanulok','Phra Nakhon Si Ayutthaya','Phrae','Phuket','Prachin Buri','Prachuap Khiri Khan','Ranong','Ratchaburi','Rayong','Roi Et','Sa Kaeo','Sakon Nakhon','Samut Prakan','Samut Sakhon','Samut Songkhram','Sara Buri','Satun','Sing Buri','Sisaket','Songkhla','Sukhothai','Suphan Buri','Surat Thani','Surin','Tak','Trang','Trat','Ubon Ratchathani','Udon Thani','Uthai Thani','Uttaradit','Yala','Yasothon'] },
    { name: 'Tobago', cities: ['--Select Your State--','Tobago'] },
    { name: 'Toga', cities: ['--Select Your State--','De La Kara','Des Plateaux','Des Savanes','Du Centre','Maritime'] },
    { name: 'Tokelau', cities: ['--Select Your State--','Atafu','Fakaofo','Nukunonu'] },
    { name: 'Tonga', cities: ['--Select Your State--','Ha"apai','Tongatapu','Vava"u'] },
    { name: 'Trinidad', cities: ['--Select Your State--','Arima','Caroni','Mayaro','Nariva','Port-of-Spain','Saint Andrew','Saint David','Saint George','Saint Patrick','San Fernando','Victoria'] },
    { name: 'Tunisia', cities: ['--Select Your State--','Ariana','Beja','Ben Arous','Bizerte','El Kef','Gabes','Gafsa','Jendouba','Kairouan','Kasserine','Kebili','Mahdia','Medenine','Monastir','Nabeul','Sfax','Sidi Bou Zid','Siliana','Sousse','Tataouine','Tozeur','Tunis','Zaghouan'] },
    { name: 'Turkey', cities: ['--Select Your State--','Adana','Adiyaman','Afyon','Agri','Aksaray','Amasya','Ankara','Antalya','Ardahan','Artvin','Aydin','Balikesir','Bartin','Batman','Bayburt','Bilecik','Bingol','Bitlis','Bolu','Burdur','Bursa','Canakkale','Cankiri','Corum','Denizli','Diyarbakir','Duzce','Edirne','Elazig','Erzincan','Erzurum','Eskisehir','Gaziantep','Giresun','Gumushane','Hakkari','Hatay','Icel','Igdir','Isparta','Istanbul','Izmir','Kahramanmaras','Karabuk','Karaman','Kars','Kastamonu','Kayseri','Kilis','Kirikkale','Kirklareli','Kirsehir','Kocaeli','Konya','Kutahya','Malatya','Manisa','Mardin','Mugla','Mus','Nevsehir','Nigde','Ordu','Osmaniye','Rize','Sakarya','Samsun','Sanliurfa','Siirt','Sinop','Sirnak','Sivas','Tekirdag','Tokat','Trabzon','Tunceli','Usak','Van','Yalova','Yozgat','Zonguldak'] },
    { name: 'Turkmenistan', cities: ['--Select Your State--','Ahal Welayaty','Balkan Welayaty','Dashhowuz Welayaty','Lebap Welayaty','Mary Welayaty'] },
    { name: 'Tuvalu', cities: ['--Select Your State--','Tuvalu'] },
    { name: 'Uganda', cities: ['--Select Your State--','Adjumani','Apac','Arua','Bugiri','Bundibugyo','Bushenyi','Busia','Gulu','Hoima','Iganga','Jinja','Kabale','Kabarole','Kalangala','Kampala','Kamuli','Kapchorwa','Kasese','Katakwi','Kibale','Kiboga','Kisoro','Kitgum','Kotido','Kumi','Lira','Luwero','Masaka','Masindi','Mbale','Mbarara','Moroto','Moyo','Mpigi','Mubende','Mukono','Nakasongola','Nebbi','Ntungamo','Pallisa','Rakai','Rukungiri','Sembabule','Soroti','Tororo'] },
    { name: 'Ukraine', cities: ['--Select Your State--','Avtonomna Respublika Krym (Simferopol)','Cherkas"ka (Cherkasy)','Chernihivs"ka (Chernihiv)','Chernivets"ka (Chernivtsi)','Dnipropetrovs"ka (Dnipropetrovs"k)','Donets"ka (Donets"k)','Ivano-Frankivs"ka (Ivano-Frankivs"k)','Kharkivs"ka (Kharkiv)','Khersons"ka (Kherson)','Khmel"nyts"ka (Khmel"nyts"kyy)','Kirovohrads"ka (Kirovohrad)','Kyyiv','Kyyivs"ka (Kiev)','L"vivs"ka (L"viv)','Luhans"ka (Luhans"k)','Mykolayivs"ka (Mykolayiv)','Odes"ka (Odesa)','Poltavs"ka (Poltava)','Rivnens"ka (Rivne)','Sevastopol','Sums"ka (Sumy)','Ternopil"s"ka (Ternopil)','Vinnyts"ka (Vinnytsya)','Volyns"ka (Luts"k)','Zakarpats"ka (Uzhhorod)','Zaporiz"ka (Zaporizhzhya)','Zhytomyrs"ka (Zhytomyr)'] },
    { name: 'United Arab Emirates', cities: ['--Select Your State--','Ajman','Abu Zaby (Abu Dhabi)','Al Fujayrah','Ash Shariqah (Sharjah)','Dubayy (Dubai)','Ra"s al Khaymah','Umm al Qaywayn'] },
    { name: 'United Kingdom', cities: ['--Select Your State--','Barking and Dagenham','Barnet','Barnsley','Bath and North East Somerset','Bedfordshire','Bexley','Birmingham','Blackburn with Darwen','Blackpool','Bolton','Bournemouth','Bracknell Forest','Bradford','Brent','Brighton and Hove','Bromley','Buckinghamshire','Bury','Calderdale','Cambridgeshire','Camden','Cheshire','City of Bristol','City of Kingston upon Hull','City of London','Cornwall','Coventry','Croydon','Cumbria','Darlington','Derby','Derbyshire','Devon','Doncaster','Dorset','Dudley','Durham','Ealing','East Riding of Yorkshire','East Sussex','Enfield','Essex','Gateshead','Gloucestershire','Greenwich','Hackney','Halton','Hammersmith and Fulham','Hampshire','Haringey','Harrow','Hartlepool','Havering','Herefordshire','Hertfordshire','Hillingdon','Hounslow','Isle of Wight','Islington','Kensington and Chelsea','Kent','Kingston upon Thames','Kirklees','Knowsley','Lambeth','Lancashire','Leeds','Leicester','Leicestershire','Lewisham','Lincolnshire','Liverpool','Luton','Manchester','Medway','Merton','Middlesbrough','Milton Keynes','Newcastle upon Tyne','Newham','Norfolk','North East Lincolnshire','North Lincolnshire','North Somerset','North Tyneside','North Yorkshire','Northamptonshire','Northumberland','Nottingham','Nottinghamshire','Oldham','Oxfordshire','Peterborough','Plymouth','Poole','Portsmouth','Reading','Redbridge','Redcar and Cleveland','Richmond upon Thames','Rochdale','Rotherham','Rutland','Salford','Sandwell','Sefton','Sheffield','Shropshire','Slough','Solihull','Somerset','South Gloucestershire','South Tyneside','Southampton','Southend-on-Sea','Southwark','St. Helens','Staffordshire','Stockport','Stockton-on-Tees','Stoke-on-Trent','Suffolk','Sunderland','Surrey','Sutton','Swindon','Tameside','Telford and Wrekin','Thurrock','Torbay','Tower Hamlets','Trafford','Wakefield','Walsall','Waltham Forest','Wandsworth','Warrington','Warwickshire','West Berkshire','West Sussex','Westminster','Wigan','Wiltshire','Windsor and Maidenhead','Wirral','Wokingham','Wolverhampton','Worcestershire','York'] },
    { name: 'Uruguay', cities: ['--Select Your State--','Artigas','Canelones','Cerro Largo','Colonia','Durazno','Flores','Florida','Lavalleja','Maldonado','Montevideo','Paysandu','Rio Negro','Rivera','Rocha','Salto','San Jose','Soriano','Tacuarembo','Treinta y Tres'] },
    { name: 'USA', cities: ['--Select Your State--','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming'] },
    { name: 'Uzbekistan', cities: ['--Select Your State--','Andijon Wiloyati','Bukhoro Wiloyati','Farghona Wiloyati','Jizzakh Wiloyati','Khorazm Wiloyati (Urganch)','Namangan Wiloyati','Nawoiy Wiloyati','Qashqadaryo Wiloyati (Qarshi)','Qoraqalpoghiston (Nukus)','Samarqand Wiloyati','Sirdaryo Wiloyati (Guliston)','Surkhondaryo Wiloyati (Termiz)','Toshkent Shahri','Toshkent Wiloyati'] },
    { name: 'Vanuatu', cities: ['--Select Your State--','Malampa','Penama','Sanma','Shefa','Tafea','Torba'] },
    { name: 'Venezuela', cities: ['--Select Your State--','Amazonas','Anzoategui','Apure','Aragua','Barinas','Bolivar','Carabobo','Cojedes','Delta Amacuro','Dependencias Federales','Distrito Federal','Falcon','Guarico','Lara','Merida','Miranda','Monagas','Nueva Esparta','Portuguesa','Sucre','Tachira','Trujillo','Vargas','Yaracuy','Zulia'] },
    { name: 'Vietnam', cities: ['--Select Your State--','An Giang','Ba Ria-Vung Tau','Bac Giang','Bac Kan','Bac Lieu','Bac Ninh','Ben Tre','Binh Dinh','Binh Duong','Binh Phuoc','Binh Thuan','Ca Mau','Can Tho','Cao Bang','Da Nang','Dac Lak','Dong Nai','Dong Thap','Gia Lai','Ha Giang','Ha Nam','Ha Noi','Ha Tay','Ha Tinh','Hai Duong','Hai Phong','Ho Chi Minh','Hoa Binh','Hung Yen','Khanh Hoa','Kien Giang','Kon Tum','Lai Chau','Lam Dong','Lang Son','Lao Cai','Long An','Nam Dinh','Nghe An','Ninh Binh','Ninh Thuan','Phu Tho','Phu Yen','Quang Binh','Quang Nam','Quang Ngai','Quang Ninh','Quang Tri','Soc Trang','Son La','Tay Ninh','Thai Binh','Thai Nguyen','Thanh Hoa','Thua Thien-Hue','Tien Giang','Tra Vinh','Tuyen Quang','Vinh Long','Vinh Phuc','Yen Bai'] },
    { name: 'Virgin Islands', cities: ['--Select Your State--','Saint Croix','Saint John','Saint Thomas'] },
    { name: 'Wales', cities: ['--Select Your State--','Blaenau Gwent','Bridgend','Caerphilly','Cardiff','Carmarthenshire','Ceredigion','Conwy','Denbighshire','Flintshire','Gwynedd','Isle of Anglesey','Merthyr Tydfil','Monmouthshire','Neath Port Talbot','Newport','Pembrokeshire','Powys','Rhondda Cynon Taff','Swansea','The Vale of Glamorgan','Torfaen','Wrexham'] },
    { name: 'Wallis and Futuna', cities: ['--Select Your State--','Alo','Sigave','Wallis'] },
    { name: 'West Bank', cities: ['--Select Your State--','West Bank'] },
    { name: 'Western Sahara', cities: ['--Select Your State--','Western Sahara'] },
    { name: 'Yemen', cities: ['--Select Your State--','Adan','Ataq','Abyan','Al Bayda','Al Hudaydah','Al Jawf','Al Mahrah','Al Mahwit','Dhamar','Hadhramawt','Hajjah','Ibb','Lahij','Ma"rib','Sa"dah','San"a"','Ta"izz'] },
    { name: 'Yugoslavia', cities: ['--Select Your State--','Kosovo','Montenegro','Serbia','Vojvodina'] },
    { name: 'Zambia', cities: ['--Select Your State--','Central','Copperbelt','Eastern','Luapula','Lusaka','North-Western','Northern','Southern','Western'] },
    { name: 'Zimbabwe', cities: ['--Select Your State--','Bulawayo','Harare','ManicalandMashonaland Central','Mashonaland East','Mashonaland West','Masvingo','Matabeleland North','Matabeleland South','Midlands'] },
  
  
  
  
  
    
  
  ];
  
  selectedcountryList:string="";

  cities: Array<any>;
changeCountry(count) {
  this.cities = this.countryList.find(con => con.name == count).cities;
}

selectedcities:string="";



employee_email:any;
employee_name:any;
employee_university:any;
employee_qualification:any;


password=""
dob=""
name = ""
email = "" 
university=""
state=""
mobile=""
qualification=""
occupation=""
confirm_password=""
capt =""
textinput=""
final=""
checkbox=""
showAge:any

/*
  name = ""
  email = ""
  Dob = ""
  university = ""
  Organization = ""
  Mobile=""
  Qualification=""
  user_name=""

  password=""
  confirm_password=""
 //Subject =""
 capt =""
 textinput=""
 final=""
 */

  constructor(private toastr: ToastrService,
    private subscribeService:SubscribeService,
    private router:Router) { }

  ngOnInit(): void {
    this.cap();
  }

  ageCalculator(){
    if(this.dob){
      const convertAge = new Date(this.dob);
      const timeDiff = Math.abs(Date.now() - convertAge.getTime());
      this.showAge = Math.floor((timeDiff / (1000 * 3600 * 24))/365);
    }
    console.log(this.showAge)
  }

  onSignUp() {
    
    
    if (this.employee_name.length == 0) {
      this.toastr.error("Please Enter Your Name")
    } else if (this.employee_email.length == 0) {
      this.toastr.error("Please Enter Email")
    
    } else if (this.dob.length == 0) {
      this.toastr.error("Please Enter Dob ")
    }
    else if (this.employee_university.length == 0) {
      this.toastr.error("Please Enter Your university ")
    }
    else if (this.mobile.length == 0) {
      this.toastr.error("Please Enter Your Contact Number ")
    }
    else if (this.employee_qualification.length == 0) {
      this.toastr.error("Please Enter Your Qualification ")
    }
    else if (this.selectedcountryList.length == 0) {
      this.toastr.error("Please Select Your  Country ")
    }
    else if (this.selectedcities.length == 0) {
      this.toastr.error("Please Select Your State ")
    }
    
    else if (this.password.length < 7) {
      this.toastr.error("Password length should not be less than 8")
    }

    else if (this.password != this.confirm_password){
      this.toastr.error("password and confirm password should be same")
    }
    else if (this.mobile.length != 10){
      this.toastr.error("please enter valid mobile number")
    }
    else if (this.capt != this.textinput){
      this.toastr.error("please enter valid captcha")
    }else if (this.checkbox.length == 0 ){
      this.toastr.error("please click the checkbox")
    }

    else if (this.checkbox.length == 0 ){
      this.toastr.error("please click the checkbox")
    }
    else if (this.showAge <=  14) {
      this.toastr.error("Entered date of birth is not valid ")
    }
    
   
    

    
    else {
      const body = {
       
  
        password:this.password,
        dob:this.dob,
        name: this.employee_name.toUpperCase(),
        email: this.employee_email,
        university: this.employee_university,
       // state: this.state,
       cities:this.selectedcities,
       countryList:this.selectedcountryList,
        mobile: this.mobile,
        qualification: this.employee_qualification,
        occupation: this.occupation,
       
     

       /*
        name: this.name.toUpperCase(),
        email: this.email,
        Organization: this.Organization.toUpperCase(),
        mob: this.selectedmobile,
     
        Dob:this.Dob,
        Qualification :this.selectedOccupation,
        Occupation :this.selectedOccupation,

        //Subject:this.Subject,
        countryList:this.selectedcountryList,
        cities:this.selectedcities
     
     
        */

        

      }
      //console.log(body)
      this.subscribeService.signUp(body).subscribe(response =>{
        if(response['status']=='success'){
          //console.log(response['data'])
          this.toastr.success("Thank You "+ " " + this.name.toUpperCase() +" "+ "For Registration"+"."+"you will receive payment link on your mail")
          this.router.navigate(['/ctf'])
          
        }
        else if(`${response['error']}`=="please enter valid email")
        {
          this.toastr.error(`${response['error']}`)
          
        }
        else if(`${response['error']}`=="user with given email is  allready present"){
          this.toastr.error(`${response['error']}`)
          
        }
       
        else{
          //this.toastr.error("user with given mobile no  is allready present")
          this.toastr.error(`${response['error']}`)
          //console.log(`${response['error']}`)
           
        }
      })

    }

  }

  /*validpass()
  {
      var stg1 = this.password;
      var stg2 = this.confirm_password;
      if(stg1==stg2)
      {
        
        return true;
      }
      else{
         
          return false;
      }
      
  }
*/
  validCap()
{
    var stg1 = this.capt;
    var stg2 = this.textinput;
    if(stg1==stg2)
    {
      
      return true;
    }
    else{
       
        return false;
    }
    console.log("hii");
}

cap(){
  var alpha =['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R',
'S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
'w','x','y','z','1','2','3','4','5','6','7','8','9','0','!','@','$','%','^','&','*'
];
var a = alpha[Math.floor(Math.random()*69)];
var b = alpha[Math.floor(Math.random()*69)];
var c = alpha[Math.floor(Math.random()*69)];
var d = alpha[Math.floor(Math.random()*69)];
var e = alpha[Math.floor(Math.random()*69)];
var f = alpha[Math.floor(Math.random()*69)];

 this.final = a+b+c+d+e+f;
this.capt = this.final;

}


}
