import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiplomaregisterFaiComponent } from './diplomaregister-fai.component';

describe('DiplomaregisterFaiComponent', () => {
  let component: DiplomaregisterFaiComponent;
  let fixture: ComponentFixture<DiplomaregisterFaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiplomaregisterFaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiplomaregisterFaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
