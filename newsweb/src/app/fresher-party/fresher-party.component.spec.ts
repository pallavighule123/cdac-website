import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FresherPartyComponent } from './fresher-party.component';

describe('FresherPartyComponent', () => {
  let component: FresherPartyComponent;
  let fixture: ComponentFixture<FresherPartyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FresherPartyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FresherPartyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
