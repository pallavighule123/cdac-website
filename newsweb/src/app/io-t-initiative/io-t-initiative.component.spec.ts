import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IoTInitiativeComponent } from './io-t-initiative.component';

describe('IoTInitiativeComponent', () => {
  let component: IoTInitiativeComponent;
  let fixture: ComponentFixture<IoTInitiativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IoTInitiativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IoTInitiativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
