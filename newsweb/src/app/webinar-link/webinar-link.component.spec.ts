import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebinarLinkComponent } from './webinar-link.component';

describe('WebinarLinkComponent', () => {
  let component: WebinarLinkComponent;
  let fixture: ComponentFixture<WebinarLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebinarLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebinarLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
