import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-webinar-link',
  templateUrl: './webinar-link.component.html',
  styleUrls: ['./webinar-link.component.css']
})
export class WebinarLinkComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  btnClick= function () {
    this.router.navigateByUrl('/registration');
};
}
