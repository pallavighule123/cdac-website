import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FutureSkillsPrimeComponent } from './future-skills-prime.component';

describe('FutureSkillsPrimeComponent', () => {
  let component: FutureSkillsPrimeComponent;
  let fixture: ComponentFixture<FutureSkillsPrimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FutureSkillsPrimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FutureSkillsPrimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
