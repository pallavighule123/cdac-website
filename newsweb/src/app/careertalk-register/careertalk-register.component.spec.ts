import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareertalkRegisterComponent } from './careertalk-register.component';

describe('CareertalkRegisterComponent', () => {
  let component: CareertalkRegisterComponent;
  let fixture: ComponentFixture<CareertalkRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareertalkRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareertalkRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
