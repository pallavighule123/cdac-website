import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NCSAMComponent } from './ncsam.component';

describe('NCSAMComponent', () => {
  let component: NCSAMComponent;
  let fixture: ComponentFixture<NCSAMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NCSAMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NCSAMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
