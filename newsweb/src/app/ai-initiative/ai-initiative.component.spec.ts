import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AiInitiativeComponent } from './ai-initiative.component';

describe('AiInitiativeComponent', () => {
  let component: AiInitiativeComponent;
  let fixture: ComponentFixture<AiInitiativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AiInitiativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiInitiativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
