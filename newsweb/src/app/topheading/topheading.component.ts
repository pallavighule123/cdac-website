import { NewsapiservicesService } from '../service/newsapiservices.service';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-topheading',
  templateUrl: './topheading.component.html',
  styleUrls: ['./topheading.component.css']
})
export class TopheadingComponent implements OnInit {

  constructor( private _services:NewsapiservicesService) { }


  topHeadingDisplay:any=[]


  ngOnInit(): void {
    this._services.topHeading().subscribe((result=>{
      console.log(result)
      //this.topHeadingDisplay=result.articles;
      this.topHeadingDisplay=result.data;
    }))
  }

}
