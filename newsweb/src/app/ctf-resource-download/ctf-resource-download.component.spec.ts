import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CtfResourceDownloadComponent } from './ctf-resource-download.component';

describe('CtfResourceDownloadComponent', () => {
  let component: CtfResourceDownloadComponent;
  let fixture: ComponentFixture<CtfResourceDownloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CtfResourceDownloadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CtfResourceDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
