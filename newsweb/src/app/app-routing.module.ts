import { InitiativeComponent } from './initiative/initiative.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { TechnewsComponent } from './technews/technews.component';
import { TopheadingComponent } from './topheading/topheading.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { CollaboratorComponent } from './collaborator/collaborator.component';
import { HistoryComponent } from './history/history.component';
import { SectorComponent } from './sector/sector.component';
import { EducationComponent } from './education/education.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { BrochureComponent } from './brochure/brochure.component';
import { WebinarLinkComponent } from './webinar-link/webinar-link.component';
import { CareertalkRegisterComponent } from './careertalk-register/careertalk-register.component';
import { Weblink2Component } from './weblink2/weblink2.component';
import { CareerCounsellingComponent } from './career-counselling/career-counselling.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { CopyrightPolicyComponent } from './copyright-policy/copyright-policy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { WebsitePoliciesComponent } from './website-policies/website-policies.component';
import { WebinarsComponent } from './webinars/webinars.component';
import { CyberInitiativeComponent } from './cyber-initiative/cyber-initiative.component';
import { IoTInitiativeComponent } from './io-t-initiative/io-t-initiative.component';
import { IntiativeUpdatedComponent } from './intiative-updated/intiative-updated.component';
import { AiInitiativeComponent } from './ai-initiative/ai-initiative.component';
import { VisualGalleryComponent } from './visual-gallery/visual-gallery.component';
import { FutureSkillsPrimeComponent } from './future-skills-prime/future-skills-prime.component';
import { ImagesComponent } from './images/images.component';
import { FresherPartyComponent } from './fresher-party/fresher-party.component';
import { CtfpageComponent } from './ctfpage/ctfpage.component';
import { DiplomapageComponent } from './diplomapage/diplomapage.component';
import { NCSAMComponent } from './ncsam/ncsam.component';
import { BlockchainComponent } from './blockchain/blockchain.component';

import { CtfregisterComponent } from './ctfregister/ctfregister.component';
import { DiplomaregisterComponent } from './diplomaregister/diplomaregister.component';
import { TestComponent } from './test/test.component';
import { DiplomaregisterSucComponent } from './diplomaregister-suc/diplomaregister-suc.component';
import { DiplomaregisterFaiComponent } from './diplomaregister-fai/diplomaregister-fai.component';
import { BlockchainregisterComponent } from './blockchainregister/blockchainregister.component';
import { CtfLoginComponent } from './ctf-login/ctf-login.component';
import { ProfileComponent } from './profile/profile.component';
import { CtfuserserviceService } from './service/ctfuserservice.service';
import { BlockchainTransactionDetailsComponent } from './blockchain-transaction-details/blockchain-transaction-details.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  
  { path:'home', component:HomeComponent},
  {path:'contact',component:ContactComponent},
  {path:'collborator',component:CollaboratorComponent},
  {path:'history',component:HistoryComponent},
  {path:'sector',component:SectorComponent},
  {path:'news',component:TopheadingComponent},
  {path:'initiative',component:InitiativeComponent},
  {path:'subscribe',component:SubscribeComponent},
  {path:'education+training',component:EducationComponent},
  {path:'brochure',component:BrochureComponent},
  {path:'career_talk',component:WebinarLinkComponent},
  {path:'registration',component:CareertalkRegisterComponent},
  {path:'carreer_talk',component: WebinarLinkComponent},
  {path:'career_counselling',component: CareerCounsellingComponent},
  {path:'privacy-policy',component: PrivacyPolicyComponent}, 
  {path:'copyright_policy',component: CopyrightPolicyComponent}, 
  {path:'terms-conditions',component: TermsConditionsComponent}, 
  {path:'website_policy',component: WebsitePoliciesComponent}, 
  {path:'webinars',component: WebinarsComponent}, 
  {path:'cyber-security',component: CyberInitiativeComponent}, 
  {path:'IoT',component: IoTInitiativeComponent}, 
 
  {path:'AI',component: AiInitiativeComponent}, 
  {path:'initiative_domain',component: IntiativeUpdatedComponent}, 
  {path:'gallery',component: VisualGalleryComponent}, 
  {path:'future-skills-prime',component: FutureSkillsPrimeComponent}, 
  {path:'images',component: ImagesComponent},
  {path:'Fimages',component:FresherPartyComponent},
  {path:'ctf',component:CtfpageComponent},
  {path:'dehcs',component:DiplomapageComponent},
  {path:'NCSAM',component:NCSAMComponent},
  {path:'blockchain',component:BlockchainComponent},
  
// {path:'ctfregister',component:CtfregisterComponent},
  {path:'diplomaregister',component:DiplomaregisterComponent},
  {path:'blockchainregister',component:BlockchainregisterComponent},
  {path:'blackcat',component:CtfLoginComponent},
  {path:'profile',component:ProfileComponent,canActivate: [CtfuserserviceService]},
  {path:'test',component:TestComponent},
  {path:'register_suc',component:DiplomaregisterSucComponent},
  {path:'register_fai',component:DiplomaregisterFaiComponent},
  {path:'btd',component:BlockchainTransactionDetailsComponent},
  // [...]
  {path: '404', component: NotfoundComponent},
  {path: '**', redirectTo: '/404'}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
