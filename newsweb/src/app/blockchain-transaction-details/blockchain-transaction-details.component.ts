import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {  BlockchainserviceService } from '../service/blockchainservice.service';
@Component({
  selector: 'app-blockchain-transaction-details',
  templateUrl: './blockchain-transaction-details.component.html',
  styleUrls: ['./blockchain-transaction-details.component.css']
})
export class BlockchainTransactionDetailsComponent implements OnInit {

  employee_email:any
  employee_name :any
  
  regid=""
  tid=""
  constructor(private router:Router, private toastr: ToastrService,  private service:BlockchainserviceService 
    ) { }

  ngOnInit(): void {
  }


  onSubmit(){

      
    if (this.regid.length == 0)
    {
      this.toastr.error("Please Enter Your Name")
    } 
    else if (this.tid.length == 0) 
    {
      this.toastr.error("Please Enter Email")
    }

    else{
      const body = {
        email:this.employee_email,
        name:this.employee_name,
        regid:this.regid,
        tid:this.tid
      }
      console.log(body)
      this.service.submitDetails(body).subscribe(response =>{
        if(response['status']=='success'){
          
          this.toastr.success("Thank You Your Details Submitted Successfully")
          this.router.navigate(['/blockchain'])
        }
        else if(`${response['error']}`=="user not exist"){
          this.toastr.error(`${response['error']}`)
        }
        else{
          //this.toastr.error("user with given mobile no  is allready present")
          this.toastr.error(`${response['error']}`)
          
         }


      })



    }

  }


}
