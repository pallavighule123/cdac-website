import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockchainregisterComponent } from './blockchainregister.component';

describe('BlockchainregisterComponent', () => {
  let component: BlockchainregisterComponent;
  let fixture: ComponentFixture<BlockchainregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockchainregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockchainregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
