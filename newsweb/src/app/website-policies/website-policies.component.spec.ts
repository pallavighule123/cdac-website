import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsitePoliciesComponent } from './website-policies.component';

describe('WebsitePoliciesComponent', () => {
  let component: WebsitePoliciesComponent;
  let fixture: ComponentFixture<WebsitePoliciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebsitePoliciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebsitePoliciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
