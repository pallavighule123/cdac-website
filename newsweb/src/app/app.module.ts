import { SubscribeService } from './service/subscribe.service';
import { DiplomaserviceService } from './service/diplomaservice.service';
import { NewsapiservicesService } from './service/newsapiservices.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopheadingComponent } from './topheading/topheading.component';
import {HttpClientModule} from '@angular/common/http';
import { TechnewsComponent } from './technews/technews.component';
import { SubscribeComponent } from './subscribe/subscribe.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { SectorComponent } from './sector/sector.component';
import { CollaboratorComponent } from './collaborator/collaborator.component';
import { HistoryComponent } from './history/history.component';
import { ContactComponent } from './contact/contact.component';
import { InitiativeComponent } from './initiative/initiative.component';
import { HomeComponent } from './home/home.component';
import {Ng2PageScrollModule} from 'ng2-page-scroll';
import { EducationComponent } from './education/education.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { BrochureComponent } from './brochure/brochure.component';
import { WebinarLinkComponent } from './webinar-link/webinar-link.component';
import { CareertalkRegisterComponent } from './careertalk-register/careertalk-register.component';
import { Weblink2Component } from './weblink2/weblink2.component';
import { CareerCounsellingComponent } from './career-counselling/career-counselling.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { WebsitePoliciesComponent } from './website-policies/website-policies.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { CopyrightPolicyComponent } from './copyright-policy/copyright-policy.component';
import { WebinarsComponent } from './webinars/webinars.component';
import { IntiativeUpdatedComponent } from './intiative-updated/intiative-updated.component';
import { CyberInitiativeComponent } from './cyber-initiative/cyber-initiative.component';
import { AiInitiativeComponent } from './ai-initiative/ai-initiative.component';
import { IoTInitiativeComponent } from './io-t-initiative/io-t-initiative.component';
import { VisualGalleryComponent } from './visual-gallery/visual-gallery.component';
import { FutureSkillsPrimeComponent } from './future-skills-prime/future-skills-prime.component';
import { ImagesComponent } from './images/images.component';
import { FresherPartyComponent } from './fresher-party/fresher-party.component';
import { CtfpageComponent } from './ctfpage/ctfpage.component';
import { DiplomapageComponent } from './diplomapage/diplomapage.component';
import { NCSAMComponent } from './ncsam/ncsam.component';
import { BlockchainComponent } from './blockchain/blockchain.component';

import { CtfregisterComponent } from './ctfregister/ctfregister.component';
import { DiplomaregisterComponent } from './diplomaregister/diplomaregister.component';
import { TestComponent } from './test/test.component';
import { DiplomaregisterSucComponent } from './diplomaregister-suc/diplomaregister-suc.component';
import { DiplomaregisterFaiComponent } from './diplomaregister-fai/diplomaregister-fai.component';
import { ApplicantDetailsComponent } from './applicant-details/applicant-details.component';
import { BlockchainregisterComponent } from './blockchainregister/blockchainregister.component';
import { BlockchainserviceService } from './service/blockchainservice.service';
import {  CtfuserserviceService } from './service/ctfuserservice.service';
import { ProfileComponent } from './profile/profile.component';
import { CtfLoginComponent } from './ctf-login/ctf-login.component';
import { BlockchainTransactionDetailsComponent } from './blockchain-transaction-details/blockchain-transaction-details.component';
import { CtfResourceDownloadComponent } from './ctf-resource-download/ctf-resource-download.component';




@NgModule({
  declarations: [
    AppComponent,
    TopheadingComponent,
    TechnewsComponent,
    SubscribeComponent,
    SectorComponent,
    CollaboratorComponent,
    HistoryComponent,
    ContactComponent,
    InitiativeComponent,
    HomeComponent,
    EducationComponent,
    NotfoundComponent,
    BrochureComponent,
    WebinarLinkComponent,
    CareertalkRegisterComponent,
    Weblink2Component,
    CareerCounsellingComponent,
    TermsConditionsComponent,
    WebsitePoliciesComponent,
    PrivacyPolicyComponent,
    CopyrightPolicyComponent,
    WebinarsComponent,
    IntiativeUpdatedComponent,
    CyberInitiativeComponent,
    AiInitiativeComponent,
    IoTInitiativeComponent,
    VisualGalleryComponent,
    FutureSkillsPrimeComponent,
    ImagesComponent,
    FresherPartyComponent,
    CtfpageComponent,
    DiplomapageComponent,
    NCSAMComponent,
    BlockchainComponent,
   
    CtfregisterComponent,
   
    DiplomaregisterComponent,
   
    TestComponent,
   
    DiplomaregisterSucComponent,
   
    DiplomaregisterFaiComponent,
    ApplicantDetailsComponent,
    BlockchainregisterComponent,
    ProfileComponent,
    CtfLoginComponent,
    BlockchainTransactionDetailsComponent,
    CtfResourceDownloadComponent,
   
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    Ng2PageScrollModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()

   

  ],
  providers: [
    NewsapiservicesService,
    SubscribeService,
    DiplomaserviceService,
    BlockchainserviceService,
    CtfuserserviceService

    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
