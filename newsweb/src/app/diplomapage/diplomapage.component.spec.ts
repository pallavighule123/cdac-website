import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiplomapageComponent } from './diplomapage.component';

describe('DiplomapageComponent', () => {
  let component: DiplomapageComponent;
  let fixture: ComponentFixture<DiplomapageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiplomapageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiplomapageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
