import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CtfuserserviceService } from '../service/ctfuserservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-ctf-login',
  templateUrl: './ctf-login.component.html',
  styleUrls: ['./ctf-login.component.css']
})
export class CtfLoginComponent implements OnInit {

  employee_email:any
  password = ''



  constructor(private router: Router, private ctfservice: CtfuserserviceService , private toastr : ToastrService) { }

  ngOnInit(): void {

  }

  onLogin() {
    console.log("inside login")

    if (this.employee_email == "saketsir@gmail.com" && this.password == "saket123") {
      this.router.navigate(['/dashboard'])
    }
    else if(this. employee_email == "saketsir@gmail.com" && this.password == "saket1234"){
      this.router.navigate(['/diplomadashboard'])
    }

    else {
        this.ctfservice
      .login(this. employee_email, this.password)
      .subscribe(response => {
        if (response['status'] == 'success') {
          const data = response['data']
          console.log(data)

          // cache the user info
          //sessionStorage['token'] = data['token']
          //sessionStorage['name'] = data['name']

          localStorage['token'] = data['token']
          localStorage['name'] = data['name']

          


          // goto the dashboard
          this.router.navigate(['/profile'])

        } else {
          this.toastr.error(`${response['error']}`)
        }
      })
    }


    /*this.ctfservice
      .login(this.username, this.password)
      .subscribe(response => {
        if (response['status'] == 'success') {
          const data = response['data']
          console.log(data)

          // cache the user info
          sessionStorage['token'] = data['token']
          sessionStorage['name'] = data['name']


          // goto the dashboard
          this.router.navigate(['/profile'])

        } else {
          console.log(response['error'])
        }
      })*/



  }



}
