import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CtfLoginComponent } from './ctf-login.component';

describe('CtfLoginComponent', () => {
  let component: CtfLoginComponent;
  let fixture: ComponentFixture<CtfLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CtfLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CtfLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
