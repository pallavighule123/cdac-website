import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';import { CtfuserserviceService } from '../service/ctfuserservice.service';
import { CommonModule } from "@angular/common";
import { Router } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  users = []
  //flagId="CPATCTF{16bf744bddbcfd81dc6a9a3f92e4f00f}"


  flagId = ""
  arr = {}
  start = {}
  points = 0
  newpoints = {}

  user1 = [{ b1: "0" }]
  user2 = [{ b1: "0" }]
  user3 = [{ b1: "0" }]
  user4 = [{ b1: "0" }]
  user5 = [{ b1: "0" }]
  user6 = [{ b1: "0" }]
  user7 = [{ b1: "0" }]
  user8 = [{ b1: "0" }]
  user9 = [{ b1: "0" }]
  user10 = [{ b1: "0" }]
  user11 = [{ b1: "0" }]
  user12 = [{ b1: "0" }]
  user13 = [{ b1: "0" }]
  user14 = [{ b1: "0" }]
  user15 = [{ b1: "0" }]
  user16 = [{ b1: "0" }]
  user17 = [{ b1: "0" }]
  user18 = [{ b1: "0" }]
  user19 = [{ b1: "0" }]
  user20 = [{ b1: "0" }]
  user21 = [{ b1: "0" }]
  user22 = [{ b1: "0" }]
  user23 = [{ b1: "0" }]
  user24 = [{ b1: "0" }]
  user25 = [{ b1: "0" }]

  f1 = 0
  f2 = 0
  f3 = 0
  f4 = 0
  f5 = 0
  f6 = 0
  f7 = 0
  f8 = 0
  f9 = 0
  f10 = 0
  f11 = 0
  f12 = 0
  f13 = 0
  f14 = 0
  f15 = 0
  f16 = 0
  f17 = 0
  f18 = 0
  f19 = 0
  f20 = 0
  f21 = 0
  f22 = 0
  f23 = 0
  f24 = 0
  f25 = 0



  constructor(private service: CtfuserserviceService, private toastr: ToastrService, private router: Router) { }

  ngOnInit(): void {
    this.loadUsers()
    this.onStart()
    this.updatedPoints()
    

  
  }




  updatedPoints()
  {
    this.service.findUpadatedPoints().subscribe(response => {
      if(response['status'] == 'success'){
        this.newpoints = response['data']
        this.points = this.newpoints['points']
      }
      else{
        console.log(response['error'])
      }

    })
  }



  loadUsers() {
    this.service.getProfile().subscribe(response => {
      if (response['status'] == 'success') {
        this.users = response['data']
        console.log(response['data'])
        //console.log(this.users[0])
      } else {
        console.log(response['error'])
      }
    })



  }










  onLogOut() {
    localStorage.removeItem("token");
    localStorage.removeItem("name");
    this.router.navigate(['/blackcat'])


  }









  onSubmitFlag() {

    if ((this.f1 == 1 && this.flagId == "CPATCTF{16bf744bddbcfd81dc6a9a3f92e4f00f}")
      ||
      (this.f2 == 1 && this.flagId == "CPATCTF{fbc2ef2eeb67f8692f85e844726fa3dc}")
      ||
      (this.f3 == 1 && this.flagId == "CPATCTF{9bbed42427692d3ace56ea9647bf07d5}")
      ||
      (this.f4 == 1 && this.flagId == "CPATCTF{8875eb87e1417f7034ca0a36d96c9439}")
      ||
      (this.f5 == 1 && this.flagId == "CPATCTF{5fb2d86f0e972b642cfbc4da0111d956}")
      ||
      (this.f6 == 1 && this.flagId == "CPATCTF{49e31d300eb9875891568cdd04167222}")
      ||
      (this.f7 == 1 && this.flagId == "CPATCTF{fa0b60c797b90594ce0804fd998cf914}")
      ||
      (this.f8 == 1 && this.flagId == "CPATCTF{9c07586d1c3653ea6f1970738403b614}")
      ||
      (this.f9 == 1 && this.flagId == "CPATCTF{40328212cdc89bb9a6104d098e8e4f39}")
      ||
      (this.f10 == 1 && this.flagId == "CPATCTF{1be5a35e85d355d6fbda4a3d63c5f59d}")

      ||
      (this.f11 == 1 && this.flagId == "CPATCTF{0e25fa99f1028b4cd518d5f0db5273bd}")

      ||
      (this.f12 == 1 && this.flagId == "CPATCTF{7f6b11397e1db930f1757197899b1132}")

      ||
      (this.f13 == 1 && this.flagId == "CPATCTF{3f1dedebcf912ac2932cf98edf38ed73}")
      ||
      (this.f14 == 1 && this.flagId == "CPATCTF{ef3ed01664ca851c15b4a71c37be6910}")

      ||
      (this.f15 == 1 && this.flagId == "CPATCTF{a83f180f39285ad9c2c1dd2cc6373dd2}")

      ||
      (this.f16 == 1 && this.flagId == "CPATCTF{11d861f6908a63294258eadd97a8d3df}")

      ||
      (this.f17 == 1 && this.flagId == "CPATCTF{43fc5c8b47e48eb9400b79d1b0bf31cc}")

      || (this.f18 == 1 && this.flagId == "CPATCTF{e56470d313e5a1bb120c948ed79c7f6e}")

      ||
      (this.f19 == 1 && this.flagId == "CPATCTF{6ce59969158bdba3633e74e407c9b925}")
      ||

      (this.f20 == 1 && this.flagId == "CPATCTF{1a737dda859cbc1da0d4ef483c1ce380}")

      ||
      (this.f21 == 1 && this.flagId == "CPATCTF{461d1b36b4a1856b149d6602d928368d}")

      ||
      (this.f22 == 1 && this.flagId == "CPATCTF{823054201f093290ecdc79c538f67f28}")
      ||
      (this.f23 == 1 && this.flagId == "CPATCTF{3ae74804648b5c287e34b2c154bb7fc0}")
      ||
      (this.f24 == 1 && this.flagId == "CPATCTF{02543c93a688702748e57665c06fae1e}")
      ||
      (this.f25 == 1 && this.flagId == "CPATCTF{f73776ec5db8a23093786f77408e5f70}")) {
      this.toastr.error("Flag is already submitted")
    }


    else {
      const body = {
        flagId: this.flagId
      }
      this.service.onSubmitFlag(body).subscribe(response => {
        if (response['status'] == 'success') {

          this.arr = response['data']
          //console.log("points")
          console.log(this.arr)
          //this.points = this.points + this.arr['points']
          //localStorage['points'] = [this.points]
          //console.log(this.points)
          console.log(this.arr["id"])



          if (this.arr['id'] == 1) 
          {

            this.user1 = [{ b1: "1" }]
            this.f1 = 1

            const body = {
              flag: 1
            }

            console.log(body)

            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
            

          }
          




          else if (this.arr['id'] == 2) 
          {

            this.user2 = [{ b1: "1" }]
            this.f2 = 1
            const body = {
              flag: 2
            }


            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })

            this.updatedPoints()

          }



          else if (this.arr['id'] == 3) 
          {
            this.user3 = [{ b1: "1" }]
            this.f3 = 1

            const body = {
              flag: 3
            }


            
            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })

            this.updatedPoints()

          }
          else if (this.arr['id'] == 4) 
          {
            this.user4 = [{ b1: "1" }]
            this.f4 = 1
            const body = {
              flag: 4
            }


            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()

          }
          else if (this.arr['id'] == 5) 
          {
            this.user5 = [{ b1: "1" }]
            this.f5 = 1
            const body = {
              flag: 5
            }

            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })

            this.updatedPoints()

          }
          else if (this.arr['id'] == 6) 
          {
            this.user6 = [{ b1: "1" }]
            this.f6 = 1
            const body = {
              flag: 6
            }

            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()

          }
          else if (this.arr['id'] == 7) 
          {
            this.user7 = [{ b1: "1" }]
            this.f7 = 1
            const body = {
              flag: 7
            }

            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 8) 
          {
            this.user8 = [{ b1: "1" }]
            this.f8 = 1
            const body = {
              flag: 8
            }

            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 9) 
          {
            this.user9 = [{ b1: "1" }]
            this.f9 = 1
            const body = {
              flag: 9
            }


            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }

          else if (this.arr['id'] == 10) 
          {
            this.user10 = [{ b1: "1" }]
            this.f10 = 1
            const body = {
              flag: 10
            }

            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }

          else if (this.arr['id'] == 11) {
            this.user11 = [{ b1: "1" }]
            this.f11 = 1
            const body = {
              flag: 11
            }

            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })

            this.updatedPoints()
          }
          else if (this.arr['id'] == 12) {
            this.user12 = [{ b1: "1" }]
            this.f12 = 1
            const body = {
              flag: 12
            }

            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }

          else if (this.arr['id'] == 13) {
            this.user13 = [{ b1: "1" }]
            this.f13 = 1
            const body = {
              flag: 13
            }


            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 14) {
            this.user14 = [{ b1: "1" }]
            this.f14 = 1
            const body = {
              flag: 14
            }

            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 15) {
            this.user15 = [{ b1: "1" }]
            this.f15 = 1
            const body = {
              flag: 15
            }

            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 16) {
            this.user16 = [{ b1: "1" }]
            this.f16 = 1
            const body = {
              flag: 16
            }


            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 17) {
            this.user17 = [{ b1: "1" }]
            this.f17 = 1
            const body = {
              flag: 17
            }
            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 18) {
            this.user18 = [{ b1: "1" }]
            this.f18 = 1
            const body = {
              flag: 18
            }


            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 19) {
            this.user19 = [{ b1: "1" }]
            this.f19 = 1

           const body = {
              flag: 19
             }
            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 20) {
            this.user20 = [{ b1: "1" }]
            this.f20 = 1
            const body = {
              flag: 20
            }
            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 21) {
            this.user21 = [{ b1: "1" }]
            this.f21 = 1
            const body = {
              flag: 21
            }
            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 22) {
            this.user22 = [{ b1: "1" }]
            this.f22 = 1
            const body = {
              flag: 22
            }
            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 23) {
            this.user23 = [{ b1: "1" }]
            this.f23 = 1
            const body = {
              flag: 23
            }
            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 24) {
            this.user24 = [{ b1: "1" }]
            this.f24 = 1
            const body = {
              flag: 24
            }
            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }
          else if (this.arr['id'] == 25) 
          {
            this.user25 = [{ b1: "1" }]
            this.f25 = 1
            const body = {
              flag: 25
            }
            this.service.updateFlagStatus(body).subscribe(response => {
              if (response['status'] == 'success') {
                this.toastr.success("updated")
              }
              else {
                this.toastr.error(response['error'])
              }
            })
            this.updatedPoints()
          }





        } else {

          //console.log(response['error'])
          console.log(response['error'])

          this.toastr.error(response['error'])
        }
      })
    }
  }




  onStart() {
    this.service.onStart().subscribe(response => {
      if (response['status'] == 'success') {
        this.start = response['data']
        console.log(this.start)
        if ((this.start['f1'] == 1)) {
          this.user1 = [{ b1: "1" }]
          this.f1 = 1
          console.log(this.f1)
          

        }

        if ((this.start['f2'] == 2)) {
          this.user2 = [{ b1: "1" }]
          this.f2 = 1
          console.log(this.f2)

        }

        if ((this.start['f3'] == 3)) {
          this.user3 = [{ b1: "1" }]
          this.f3 = 1
          console.log(this.f3)

        }

        if ((this.start['f4'] == 4)) {
          this.user4 = [{ b1: "1" }]
          this.f4 = 1
          console.log(this.f4)

        }
        if ((this.start['f5'] == 5)) {
          this.user5 = [{ b1: "1" }]
          this.f5 = 1
          console.log(this.f5)

        }
        if ((this.start['f6'] == 6)) {
          this.user6 = [{ b1: "1" }]
          this.f6 = 1
          console.log(this.f6)

        }

        if ((this.start['f7'] == 7)) {
          this.user7 = [{ b1: "1" }]
          this.f7 = 1
          console.log(this.f7)

        }
        if ((this.start['f8'] == 8)) {
          this.user8 = [{ b1: "1" }]
          this.f8 = 1
          console.log(this.f8)

        }
        if ((this.start['f9'] == 9)) {
          this.user9 = [{ b1: "1" }]
          this.f9 = 1
          console.log(this.f9)

        }
        if ((this.start['f10'] == 10)) {
          this.user10 = [{ b1: "1" }]
          this.f10 = 1
          console.log(this.f9)

        }
        if ((this.start['f11'] == 11)) {
          this.user11 = [{ b1: "1" }]
          this.f11 = 1
          console.log(this.f11)

        }
        if ((this.start['f12'] == 12)) {
          this.user12 = [{ b1: "1" }]
          this.f12 = 1
          console.log(this.f12)

        }
        if ((this.start['f13'] == 13)) {
          this.user13 = [{ b1: "1" }]
          this.f13 = 1
          console.log(this.f13)

        }
        if ((this.start['f14'] == 14)) {
          this.user14 = [{ b1: "1" }]
          this.f14 = 1
          console.log(this.f14)

        }

        if ((this.start['f15'] == 15)) {
          this.user15 = [{ b1: "1" }]
          this.f15 = 1
          console.log(this.f15)

        }

        if ((this.start['f16'] == 16)) {
          this.user16 = [{ b1: "1" }]
          this.f16 = 1
          console.log(this.f16)

        }
         
        if ((this.start['f17'] == 17)) {
          this.user17 = [{ b1: "1" }]
          this.f17 = 1
          console.log(this.f17)

        }

        if ((this.start['f18'] == 18)) {
          this.user18 = [{ b1: "1" }]
          this.f18 = 1
          console.log(this.f18)

        }

        if ((this.start['f19'] == 19)) {
          this.user19 = [{ b1: "1" }]
          this.f19 = 1
          console.log(this.f19)

        }
        if ((this.start['f20'] == 20)) {
          this.user20 = [{ b1: "1" }]
          this.f20 = 1
          console.log(this.f20)

        }
        if ((this.start['f21'] == 21)) {
          this.user21 = [{ b1: "1" }]
          this.f21 = 1
          console.log(this.f21)

        }
        if ((this.start['f22'] == 22)) {
          this.user22 = [{ b1: "1" }]
          this.f22 = 1
          console.log(this.f22)

        }
        if ((this.start['f23'] == 23)) {
          this.user23 = [{ b1: "1" }]
          this.f23 = 1
          console.log(this.f23)

        }
        if ((this.start['f24'] == 24)) {
          this.user24 = [{ b1: "1" }]
          this.f24 = 1
          console.log(this.f24)

        }
        if ((this.start['f25'] == 25)) {
          this.user25 = [{ b1: "1" }]
          this.f25 = 1
          console.log(this.f25)

        }
        









      }
      else {
        console.log(response['error'])

        this.toastr.error(response['error'])
      }
    })
  }
 






  

 
  





 


    

   
    

  
   

}
